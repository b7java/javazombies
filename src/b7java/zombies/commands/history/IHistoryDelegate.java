package b7java.zombies.commands.history;

public interface IHistoryDelegate {
	void process(History history);
}
