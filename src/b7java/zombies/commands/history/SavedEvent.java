package b7java.zombies.commands.history;

import b7java.zombies.commands.events.IEvent;

public class SavedEvent {
	public IEvent getEvent() {
		return event;
	}
	public void setEvent(IEvent event) {
		this.event = event;
	}
	public float getTime() {
		return time;
	}
	public void setTime(float time) {
		this.time = time;
	}
	
	public SavedEvent(IEvent event, float time) {
		super();
		this.event = event;
		this.time = time;
	}

	public SavedEvent() {
		super();
	}

	public IEvent event;
	public float time;
}
