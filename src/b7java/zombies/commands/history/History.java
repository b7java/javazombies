package b7java.zombies.commands.history;

import java.util.ArrayList;

public class History {
	public History() {
		super();
	}

	ArrayList<SavedEvent> events = new ArrayList<>();

	public ArrayList<SavedEvent> getEvents() {
		return events;
	}

	public void setEvents(ArrayList<SavedEvent> events) {
		this.events = events;
	}
}
