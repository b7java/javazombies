package b7java.zombies.commands.history;

import java.awt.event.KeyEvent;

import b7java.zombies.actions.IAction;
import b7java.zombies.commands.IEventDelegate;
import b7java.zombies.commands.events.IEvent;
import b7java.zombies.commands.events.IEventKey;
import b7java.zombies.commands.events.KeyboardEventKey;

public class HistorySaver implements IEventDelegate, IAction{

	@Override
	public void onEvent(IEvent event) {
		if (isSavingCmd(event))
		{
			saveDelegate.process(history);;
			return;
		}
		SavedEvent se = new SavedEvent(event, currentTime);
		history.getEvents().add(se);
	}

	@Override
	public State step(float dtSec) {
		currentTime += dtSec;
		return State.ACTIVE;
	}
	
	public History getHistory() {
		return history;
	}
	
	boolean isSavingCmd(IEvent event){
		return saveDelegate != null && 
				saveCommandKey != null && event.getKey().equals(saveCommandKey);
	}

	public IEventKey getSaveCommandKey() {
		return saveCommandKey;
	}

	public void setSaveCommandKey(IEventKey saveCommandKey) {
		this.saveCommandKey = saveCommandKey;
	}

	public IHistoryDelegate getSaveDelegate() {
		return saveDelegate;
	}

	public void setSaveDelegate(IHistoryDelegate saveDelegate) {
		this.saveDelegate = saveDelegate;
	}
	
	History history = new History();
	float currentTime = 0;
	
	IEventKey saveCommandKey;
	IHistoryDelegate saveDelegate;
}
