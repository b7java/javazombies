package b7java.zombies.commands;

import b7java.zombies.commands.events.IEvent;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Command, which runs at a moment of some IEvent rising.
 * For example, when a hot key pressed.
 */
@FunctionalInterface
public interface ICommand {
	/** 
	 * Runs command (exists in Runnable)
	 * @param event - event information
	 */
	void run(IEvent event);
	
	/** 
	 * Is enabled. Can be used in menu, action bars...
	 */
	default boolean isEnabled() {return true;}
}
