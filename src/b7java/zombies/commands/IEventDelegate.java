package b7java.zombies.commands;

import b7java.zombies.commands.events.IEvent;

public interface IEventDelegate {
	void onEvent(IEvent event);	
}
