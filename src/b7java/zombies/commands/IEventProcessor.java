package b7java.zombies.commands;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import b7java.zombies.commands.events.IEvent;
import b7java.zombies.commands.events.IEventKey;
import b7java.zombies.exceptions.InvalidConfigException;
import b7java.zombies.commands.events.Event;
import b7java.zombies.commands.events.KeyboardEventKey;
import b7java.zombies.commands.events.MouseEventKey;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Contains commands, binds them to hot keys etc.
 */
public interface IEventProcessor {
	/**
	 * Adds a command.
	 * @param  id - command id string, used in binding
	 * @param  command - a command 
	 * @throws if a command with this id already exists
	 */
	void add(String id, ICommand command) throws InvalidConfigException;
	
	/**
	 * gets a command by id.
	 * @param  id - command id string
	 * return  command with id, or null
	 */
	ICommand get(String id);	
	
	/**
	 * binds a command with commandId to the event.
	 * One command can be binded to many events, many commands can be added to one event
	 * @param  commandId - command id 
	 */	
	void bind(String commandId, IEventKey event);
	
	/**
	 * This command will run if nothing will binded to the event.
	 * We can add some default commands, all they will run.
	 * @param  commandId - command id 
	 */		
	void addDefaultCommand(ICommand command);
	
	/**
	 * Runs all commands, binded to the event.
	 * One command can be binded to many events, many commands can be added to one event
	 * @param  event - event, which commands are to run 
	 */	
	void onEvent(IEvent event);	
	
	/**
	 * Runs all commands, binded to the key.
	 * @param  keyEvent - awt keyboard event 
	 */		
	default void onKeyboard(KeyEvent keyEvent){
		KeyboardEventKey eventKey = new KeyboardEventKey(keyEvent);
		Event event = new Event(eventKey);
		onEvent(event);		
	}

	default void onMouse(MouseEvent mouseEvent){
		MouseEventKey eventKey = new MouseEventKey(mouseEvent);
		Event event = new Event(eventKey);
		onEvent(event);
	}
	
	void addDelegate(IEventDelegate delegate);
}
