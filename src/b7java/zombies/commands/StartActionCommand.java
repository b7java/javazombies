package b7java.zombies.commands;

import b7java.zombies.actions.IAction;
import b7java.zombies.commands.events.IEvent;
import b7java.zombies.entities.IGame;

public class StartActionCommand implements ICommand {

	IAction action;
	IGame game;
	
	public StartActionCommand(IAction action, IGame game) {
		super();
		this.action = action;
		this.game = game;
	}

	@Override
	public void run(IEvent event) {
		game.add(action);
	}

}
