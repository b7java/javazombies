package b7java.zombies.commands.events;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 * Key of an Event, which will be used in binding.
 * For example, when a hot key pressed.
 */
@JsonTypeInfo(use=Id.CLASS)
public interface IEventKey {

}
