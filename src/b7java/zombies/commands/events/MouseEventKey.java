package b7java.zombies.commands.events;

import java.awt.event.MouseEvent;

public class MouseEventKey implements IEventKey {

	public MouseEventKey() {
		super();
	}

	public int getButton() {
		return button;
	}

	public void setButton(int button) {
		this.button = button;
	}

	private int button;
	//private int modifiers=0;

	public MouseEventKey(int button) {
		super();
		this.button = button;

	}

	public MouseEventKey(MouseEvent mouseEvent) {
		button = mouseEvent.getButton(); // which button has changed state
		//modifiers = mouseEvent.getModifiersEx(); // modal keys state
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + button;
		//result = prime * result + modifiers;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MouseEventKey other = (MouseEventKey) obj;
		if (button != other.button)
			return false;
		/*if (modifiers != other.modifiers)
			return false;*/
		return true;
	}

	
}
