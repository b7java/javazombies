package b7java.zombies.commands.events;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

public class Event implements IEvent {

	@Override
	public String toString() {
		return "Event [key=" + key + ", clientId=" + clientId + "]";
	}

	@Override
	public IEventKey getKey() {
		return key;
	}

	public Event(IEventKey eventKey) {
		super();
		this.key = eventKey;
	}

	IEventKey key;
	String clientId;

	@Override
	public String getClientId() {
		return clientId;
	}

	@Override
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public void setKey(IEventKey key) {
		this.key = key;
	}

	public Event() {
		super();
	}
}
