package b7java.zombies.commands.events;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class KeyboardEventKey implements IEventKey {

	@Override
	public String toString() {
		return "KeyboardEventKey [keyCode=" + keyCode + ", modifiers=" + modifiers + "]";
	}

	public KeyboardEventKey(int keyCode) {
		super();
		this.keyCode = keyCode;
	}	

	public KeyboardEventKey() {
		super();
	}

	public int getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(int keyCode) {
		this.keyCode = keyCode;
	}

	public int getModifiers() {
		return modifiers;
	}

	public void setModifiers(int modifiers) {
		this.modifiers = modifiers;
	}

	public KeyboardEventKey(int keyCode, boolean alt, boolean cntrl, boolean shift) {
		super();
		this.keyCode = keyCode;
		this.modifiers = 0;
		if (alt){
			this.modifiers |= InputEvent.ALT_DOWN_MASK;
		}
		if (cntrl){
			this.modifiers |= InputEvent.CTRL_DOWN_MASK;
		}
		if (shift){
			this.modifiers |= InputEvent.SHIFT_DOWN_MASK;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + keyCode;
		result = prime * result + modifiers;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyboardEventKey other = (KeyboardEventKey) obj;
		if (keyCode != other.keyCode)
			return false;
		if (modifiers != other.modifiers)
			return false;
		return true;
	}

	public KeyboardEventKey(KeyEvent keyEvent) {
		keyCode = keyEvent.getKeyCode();
		modifiers = keyEvent.getModifiersEx();
	}
	
	static boolean checkFlag(int patternModifiers, int eventModifiers, int flag){
		return (patternModifiers & flag) <= (eventModifiers & flag);
	}
	
	static boolean checkModifiers(int patternModifiers, int eventModifiers){
		if (patternModifiers == eventModifiers){
			return true;
		}
		
		int [] flags = {InputEvent.SHIFT_DOWN_MASK, InputEvent.CTRL_DOWN_MASK, InputEvent.ALT_DOWN_MASK};
		for(int flag : flags ){
			if (!checkFlag(patternModifiers, eventModifiers, flag)){
				return false;
			}			
		}

		return true;
	}
	
	int keyCode;
	int modifiers = 0;
}
