package b7java.zombies.commands.events;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Event, which can evoke some commands.
 * For example, when a hot key pressed.
 */
@JsonTypeInfo(use=Id.CLASS)
public interface IEvent {
	IEventKey getKey();
	void setKey(IEventKey rey);
	
	String getClientId();
	void setClientId(String clientId);
}
