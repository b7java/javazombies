package b7java.zombies.commands;

import b7java.zombies.geom.Point;
import b7java.zombies.pass.IPassability;

import java.awt.event.KeyEvent;

import b7java.zombies.commands.events.IEvent;
import b7java.zombies.commands.events.KeyboardEventKey;
import b7java.zombies.connection.IClient;
import b7java.zombies.entities.IEntity;
import b7java.zombies.exceptions.InvalidConfigException;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Moves an entity if it's clientId is equal with the command's client id
 * 
 *
 */

public class MoveFromClientCommand implements ICommand {

	/** Client id */
	String clientId;
	
	/** Moved entity */	
	IEntity entity;
	
	/** Move step */		
	double		dx, dy;
	
	IPassability passability;
	
	public MoveFromClientCommand(String clientId, IEntity entity, 
								 double dx, double dy,
								 IPassability passability) {
		super();
		this.clientId = clientId;
		this.entity = entity;
		this.dx = dx;
		this.dy = dy;
		this.passability = passability;
	}

	@Override
	public void run(IEvent event) {
		if (clientId == null || IClient.compareIDs(event.getClientId(), clientId)){
			Point p = entity.getPoint();
			Point to = new Point(p.x + dx, p.y + dy);
			if (passability == null || passability.enabled(p, to)){
				entity.setPoint(to);
			}
		}
	}
	
	/**
	 * Adds to a processor 4 commands of entity moving and binds this commands to the arrow keys
	 * @param processor - processor 
	 * @param clientId - client Id, command will move entity only if Event is from has the same client id
	 * @param entity - moved entity
	 * @param step  - step in every direction
	 * @throw InvalidConfigException if there is a command id redefinition
	 */
	
	public static void addAllDirections(IEventProcessor processor, String clientId, IEntity entity, double step) throws InvalidConfigException{
		addAllDirections(processor, clientId,
				entity, step,
				null);		
	}
	
	public static void addAllDirections(IEventProcessor processor, String clientId,
										IEntity entity, double step,
										IPassability passibility) throws InvalidConfigException{
		MoveFromClientCommand right = new MoveFromClientCommand(clientId, entity, step, 0, passibility);
		processor.add("right" + clientId, right);
		processor.bind("right" + clientId, new KeyboardEventKey(KeyEvent.VK_RIGHT));
		
		MoveFromClientCommand left = new MoveFromClientCommand(clientId, entity, -step, 0, passibility);
		processor.add("left" + clientId, left);
		processor.bind("left" + clientId, new KeyboardEventKey(KeyEvent.VK_LEFT));		
		
		MoveFromClientCommand up = new MoveFromClientCommand(clientId, entity, 0, -step, passibility);
		processor.add("up" + clientId, up);
		processor.bind("up" + clientId, new KeyboardEventKey(KeyEvent.VK_UP));
		
		MoveFromClientCommand down = new MoveFromClientCommand(clientId, entity, 0, step, passibility);
		processor.add("down" + clientId, down);
		processor.bind("down" + clientId, new KeyboardEventKey(KeyEvent.VK_DOWN));			
	}
}
