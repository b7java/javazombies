package b7java.zombies.commands;

import b7java.zombies.geom.Point;

import b7java.zombies.commands.events.IEvent;
import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IGame;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Logs the current game state.
 */
public class LogGameCommand implements ICommand {

	@Override
	public void run(IEvent event) {
		log("LogGameCommand");
		log("{");
		game.forEarchEntity(this::log);
		log("}");

	}

	/**
	* Logs entity.
	* @param entity - logged entity
	*/		
	void log(IEntity entity){
		Point p = entity.getPoint();
		log(entity.getName() + ": " + p.x + ", " + p.y);
	}
	
	/**
	* Logs a string.
	* @param str - logged string
	*/		
	void log(String str){
		System.out.println(str);
	}
	
	/**
	 * Logged game.
	 */	
	IGame game;

	public LogGameCommand(IGame game) {
		super();
		this.game = game;
	}
}
