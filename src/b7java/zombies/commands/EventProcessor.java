package b7java.zombies.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import b7java.zombies.commands.ICommand;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.events.IEvent;
import b7java.zombies.commands.events.IEventKey;
import b7java.zombies.exceptions.InvalidConfigException;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * The IEventProcessor implementation.
 */
public class EventProcessor implements IEventProcessor {

	@Override
	public void add(String id, ICommand command) throws InvalidConfigException {
		if (command == null)
			throw new InvalidConfigException(InvalidConfigException.Problem.COMMAND_NULL_POINTER,
											 "Command is null, id: " + id);		
		if (id2Commands.containsKey(id))
			throw new InvalidConfigException(InvalidConfigException.Problem.COMMAND_IDS_REDEFENITION,
											 "Command id redefinition: " + id);
		id2Commands.put(id, command);								 
	}

	@Override
	public ICommand get(String id) {
		return id2Commands.get(id);
	}

	@Override
	public void bind(String commandId, IEventKey event) {
		List<String> ids = binds.get(event);
		if (ids == null){
			ids = new ArrayList<>();
			binds.put(event, ids);
		}
		if (!ids.contains(commandId)){
			ids.add(commandId);
		}
	}

	@Override
	public void onEvent(IEvent event) {
		List<String> ids = binds.get(event.getKey());
		if (ids != null){
			for(String id : ids){
				ICommand command = get(id);
				if (command.isEnabled()){
					command.run(event);
				}
			}
		}
		else{
			for(ICommand command : defaultCommands){
				command.run(event);
			}
		}
		
		delegates.forEach((sub)->sub.onEvent(event));
	}
	

	@Override
	public void addDefaultCommand(ICommand command) {
		defaultCommands.add(command);
	}
	

	@Override
	public void addDelegate(IEventDelegate delegate) {
		delegates.add(delegate);
	}		
	/**
	 * String id -> ICommand map.
	 */
	Map<String, ICommand> id2Commands = new HashMap<>();
	
	/**
	 * IEventKey -> command id's map.
	 */	
	Map<IEventKey, List<String>> binds = new HashMap<>();
	
	/**
	 * Default commands.
	 */	
	List<ICommand> defaultCommands = new ArrayList<>();	
	
	/**
	 * SubProcessors.
	 */	
	List<IEventDelegate> delegates = new ArrayList<>();
}
