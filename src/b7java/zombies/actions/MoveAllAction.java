package b7java.zombies.actions;

import b7java.zombies.geom.Point;

import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IGame;

public class MoveAllAction implements IAction {

	@Override
	public State step(float dtSec) {
		timeLeft -= dtSec;
		if (timeLeft <= 0)
		{
			apply();
			timeLeft = deltaT;
		}
		return State.ACTIVE;
	}

	private void apply() {
		game.forEarchEntity(this::applyToEntity);
	}
	
	void applyToEntity(IEntity entity)
	{
		Point p = entity.getPoint();
		if (p.x > 0)
			entity.setPoint(new Point(p.x-1, p.y));
	}

	public MoveAllAction(float deltaT, IGame game) {
		super();
		this.deltaT = deltaT;
		this.game = game;
	}
	float deltaT;
	float timeLeft = 0;
	IGame game;
}
