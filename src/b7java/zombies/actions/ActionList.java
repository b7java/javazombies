package b7java.zombies.actions;

import java.util.ArrayList;

public class ActionList implements IAction {

	@Override
	public State step(float dtSec) {
		for(int i = 0; i < actions.size(); )
		{
			if (actions.get(i).step(dtSec) == State.TO_BE_DELETED){
				actions.remove(i);
			}
			else {
				i++;
			}
		}
		return actions.isEmpty() ? State.TO_BE_DELETED : State.ACTIVE;
	}
	
	public final ArrayList<IAction> getActions() {
		return actions;
	}

	public void add(IAction action){
		actions.add(action);
	}

	ArrayList<IAction> actions = new ArrayList<>();
}
