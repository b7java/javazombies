package b7java.zombies.actions;

@FunctionalInterface
public interface IAction {
	
	public enum State{
		ACTIVE,
		TO_BE_DELETED
	};
	
	State step(float dtSec);
}
