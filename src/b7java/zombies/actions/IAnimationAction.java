package b7java.zombies.actions;

import b7java.zombies.images.SpriteData;

public interface IAnimationAction extends IAction {
	SpriteData	getImage();
}
