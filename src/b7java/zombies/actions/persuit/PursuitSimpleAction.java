package b7java.zombies.actions.persuit;

import java.util.List;

import b7java.zombies.actions.IAction;
import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IGame;
import b7java.zombies.geom.Point;
import b7java.zombies.geom.Vector;
import b7java.zombies.pass.IPassability;

public class PursuitSimpleAction implements IAction {

	public PursuitSimpleAction(IEntity entity, IPersuitTarget target, double velocity) {
		super();
		this.entity = entity;
		this.target = target;
		this.velocity = velocity;
	}

	public PursuitSimpleAction(IEntity entity, IEntity targetEntity, double velocity) {
		this(entity, new PersuitTargetEntity(targetEntity), velocity);
	}

	IEntity entity;
	IPersuitTarget target;
	double velocity;
	IPassability passability;
	
	
	public IPassability getPassability() {
		return passability;
	}

	public void setPassability(IPassability passability) {
		this.passability = passability;
	}

	@Override
	public State step(float dtSec) {
		
		Point p1 = target.getPoint();
		Point p0 = entity.getPoint();
		if (p0.equals(p1)){
			return State.ACTIVE;
		}
		Vector v = calculateVector(p1, p0, dtSec);
		if (v != null){
			Point p = p0.moveByVector(v);
			entity.setPoint(p);
		}
		return State.ACTIVE;
	}

	private Vector calculateVector(Point p1, Point p0, float dtSec) {
		double dx = p1.x - p0.x;
		double dy = p1.y - p0.y;
		
		Vector [] v = new Vector[2];
		Vector vx = new Vector(Math.signum(dx), 0);
		Vector vy = new Vector(0, Math.signum(dy));		
		if (Math.abs(dx) > Math.abs(dy)){
			v[0] = vx;
			v[1] = vy;
		}
		else{
			v[0] = vy;
			v[1] = vx;
		}
		
		for (Vector step : v){
			
			step.scale(velocity * dtSec);		
			if(passability.enabled(p0, step))
			{
				return step;
			}
		}
		return null;
	}
	
	public static void addActions(List<IEntity> entities, IGame game, IPassability passability, IEntity target, double velocity){
		
		IPersuitTarget persuitTarget = new PersuitTargetEntity(target);
		
		for (IEntity entity : entities){
			PursuitSimpleAction action = new PursuitSimpleAction(entity, persuitTarget, velocity );
			action.setPassability(passability);
			game.add(action);
		}
	}

}
