package b7java.zombies.actions.persuit;

import b7java.zombies.entities.IEntity;
import b7java.zombies.geom.Point;

public class PersuitTargetEntity implements IPersuitTarget {

	IEntity entity;
	
	public PersuitTargetEntity(IEntity entity) {
		super();
		this.entity = entity;
	}

	@Override
	public Point getPoint() {
		return entity.getPoint();
	}

}
