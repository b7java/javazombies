package b7java.zombies.actions.persuit;

import b7java.zombies.geom.Point;

public interface IPersuitTarget {
	Point getPoint();
}
