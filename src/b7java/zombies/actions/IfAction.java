package b7java.zombies.actions;

import b7java.zombies.predicates.IPredicateAction;

public class IfAction implements IAction {

	IPredicateAction predicate;
	ActionList actionsIf = new ActionList();
	ActionList actionsElse = new ActionList();
	
	@Override
	public State step(float dtSec) {
		State result = predicate.step(dtSec);
		if (predicate.is()){
			actionsIf.step(dtSec);
		}
		else{
			actionsElse.step(dtSec);
		}
		return result;
	}
	
	public IfAction(IPredicateAction predicate) {
		super();
		this.predicate = predicate;
	}

	void addActionIf(IAction action){
		actionsIf.add(action);
	}
	
	void addActionElse(IAction action){
		actionsElse.add(action);
	}	
}
