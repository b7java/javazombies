package b7java.zombies.actions;

import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IGame;
import b7java.zombies.geom.Axis;
import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.IAnimation;
import b7java.zombies.predicates.IPredicateAction;
import b7java.zombies.predicates.PredicateMove;
import b7java.zombies.predicates.PredicateMoveByAxis;

public class AnimationHelper {

	public static void addMoveDirectionAnimation(IEntity entity, IGame game,
												 IAnimation animation, AnimationPlayingSetting settings,
												 Axis axis, int direction){
		if (animation == null){
			return;
		}
		IPredicateAction predicate = new PredicateMoveByAxis(entity, axis, direction);
		IAnimationAction animationAction = new AnimationAction(entity, animation, settings);
		IfAction ifAction = new IfAction(predicate);
		ifAction.addActionIf(animationAction);
		game.add(ifAction);

	}	
	public static void addMoveAnimations(IEntity entity, IGame game,
										 IAnimation left, IAnimation right,
										 IAnimation up, IAnimation down,
										 float timeStepCoeff){
		
		AnimationPlayingSetting settings = new AnimationPlayingSetting(AnimationPlayingSetting.Mode.LOOP);
		settings.dtSec *= timeStepCoeff;
		addMoveDirectionAnimation(entity, game, left, settings, Axis.X, -1);
		addMoveDirectionAnimation(entity, game, right, settings,Axis.X, 1);
		addMoveDirectionAnimation(entity, game, up, settings, Axis.Y, -1);
		addMoveDirectionAnimation(entity, game, down, settings, Axis.Y, 1);
	}
	
}
