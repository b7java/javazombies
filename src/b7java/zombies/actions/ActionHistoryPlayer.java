package b7java.zombies.actions;

import java.util.ArrayList;

import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.events.IEvent;
import b7java.zombies.commands.history.History;
import b7java.zombies.commands.history.SavedEvent;
import b7java.zombies.entities.IGame;

public class ActionHistoryPlayer implements IAction {

	public ActionHistoryPlayer(History history, IGame game) {
		super();
		this.history = history;
		this.proc = game.getEventProcessor();
	}

	@Override
	public State step(float dtSec) {
		currentTime += dtSec;
		return playEvents();
	}

	State playEvents(){
		ArrayList<SavedEvent> events = history.getEvents();
		for (;nextEvent < events.size();){
			SavedEvent event = events.get(nextEvent);
			if (event.getTime() > currentTime)
				break;
			play(event.getEvent());
			nextEvent++;
		}
		return nextEvent < events.size() ? State.ACTIVE : State.TO_BE_DELETED;
	}
	
	private void play(IEvent event) {
		proc.onEvent(event);
	}

	History history;
	IEventProcessor proc;
	float currentTime = 0;
	int   nextEvent = 0;
}
