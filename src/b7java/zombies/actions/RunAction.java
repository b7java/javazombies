package b7java.zombies.actions;

import b7java.zombies.geom.Point;

import b7java.zombies.actions.IAction;
import b7java.zombies.entities.Entity;

// import statements
/**
 * @author Kovalenko Ivan <radjawarior @ mail.ru>
 * @version 1.0
 * @since 1.1
 */
public class RunAction implements IAction {
	// class body
	float overTime = 0;
	/** The object contains coordinates of the starting point and name. */
	Entity entity;
	/** Endpoint coordinates */
	Point point;
	/** Travel time */
	float timerun;
	/** X-axis speed */
	double speedX;
	/** Y-axis speed */
	double speedY;
	/** Place of start of the object along the X axis */
	double startX;
	/** Place of start of the object along the Y axis */
	double startY;
	/** The finish point of the object along the X axis */
	double x1;
	/** The finish point of the object along the Y axis */
	double y1;

	/**
	 * The constructor RunAction takes an entity, point and float timerun (1)
	 * <p>
	 * Entity entity - An object with initial coordinates. (2)
	 * <p>
	 * Point point - An object with finite coordinates.
	 * <p>
	 * float timerun - Travel time.
	 * <p>
	 */
	public RunAction(Entity entity, Point point, float timerun) {
		// constructor body
		super();
		this.entity = entity;
		this.point = point;
		this.timerun = timerun;
		Point currentPoint = entity.getPoint();
		double x0 = currentPoint.getX();
		double y0 = currentPoint.getY();
		this.startX = x0;
		this.startY = y0;
		this.x1 = point.getX();
		this.y1 = point.getY();
		speedX = (x1 - x0) / timerun;
		speedY = (y1 - y0) / timerun;

	}

	/**
	 * The method finds the coordinates of the object through the delta of time. (1)
	 * <p>
	 * The method receives the delta of time and uses the speed of (2)
	 * <p>
	 * X and the speed of Y, the initial and
	 * <p>
	 * final coordinates, finds and moves our coordinates to the found coordinates.
	 * <p>
	 * If the travel time is 0, then we put the object in the first step at the end
	 * point and say that the
	 * <p>
	 * mead has finished the work.
	 * <p>
	 * If the object after n steps is already at the end point, then we exit and say
	 * that the method has
	 * <p>
	 * completed its work.
	 * <p>
	 * If the first two conditions are not approached, then we find the coordinates
	 * in accordance with
	 * <p>
	 * the input data and rearrange the object to the desired point and say that the
	 * method is in the ACTIVE state.
	 * <p>
	 * 
	 * @return State of the method(TO_BE_DELETED end ACTIVE). (3)
	 */
	@Override
	public State step(float dtSec) {
		// method body with a return statement
		Point currentPoint = entity.getPoint();
		if (timerun == 0) {
			currentPoint.setLocation(x1, y1);
			return State.TO_BE_DELETED;
		}
		if (currentPoint.equals(point))
			return State.TO_BE_DELETED;
		overTime += dtSec;

		double x = startX + overTime * speedX;
		double y = startY + overTime * speedY;
		currentPoint.setLocation(x, y);
		return State.ACTIVE;
	}
}
