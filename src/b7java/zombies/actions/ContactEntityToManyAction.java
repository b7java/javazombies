package b7java.zombies.actions;

import b7java.zombies.contact.IEntityContactHander;
import b7java.zombies.contact.IEntityContactManager;
import b7java.zombies.entities.IEntity;

public class ContactEntityToManyAction implements IAction {

	IEntityContactManager contactManager;
	IEntity entity;
	IEntityContactHander handler;
	boolean removeTargetEntities; 
	
	public ContactEntityToManyAction(IEntityContactManager contactManager, IEntity entity, IEntityContactHander handler,
			boolean removeTargetEntities) {
		super();
		this.contactManager = contactManager;
		this.entity = entity;
		this.handler = handler;
		this.removeTargetEntities = removeTargetEntities;
	}
	
	@Override
	public State step(float dtSec) {
		if (contactManager.isEmpty()){
			return State.TO_BE_DELETED;
		}
		contactManager.contactTargets(entity, handler, removeTargetEntities);
		return contactManager.isEmpty() ? State.TO_BE_DELETED : State.ACTIVE;
	}

}
