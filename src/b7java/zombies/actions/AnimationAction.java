package b7java.zombies.actions;

import b7java.zombies.entities.IEntity;
import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.AnimationPlayingSetting.Mode;
import b7java.zombies.images.IAnimation;
import b7java.zombies.images.SpriteData;

public class AnimationAction implements IAnimationAction {

	IEntity entity;
	IAnimation animation;
	AnimationPlayingSetting settings;
	float time = 0;
	boolean isStarted = false;
	int currentFrame = -1;
	
	public AnimationAction(IEntity entity, IAnimation animation, AnimationPlayingSetting settings) {
		super();
		this.entity = entity;
		this.animation = animation;
		this.settings = settings;
	}
	
	public AnimationAction(IAnimation animation, AnimationPlayingSetting settings) {
		super();
		this.animation = animation;
		this.settings = settings;
	}	

	@Override
	public State step(float dtSec) {
		time = isStarted ? time + dtSec : 0;
		isStarted = true;
		if (entity != null){
			stepEntity();
		}
		else{
			currentFrame = calculateFrame();
		}
		return isEnd() ? State.TO_BE_DELETED : State.ACTIVE;
	}

	private void stepEntity() {
		int frame = calculateFrame();
		if (frame != currentFrame){
			currentFrame = frame;	
			SpriteData spriteData = animation.getFrame(frame);
			entity.setSpriteData(spriteData );
		}
	}

	private boolean isEnd() {
		return currentFrame >= animation.getFramesCount() - 1 
			   && settings.mode != Mode.LOOP;
	}

	private int calculateFrame() {
		int nearest = Math.round(time / settings.dtSec);
		if (settings.mode == Mode.LOOP)
			return nearest % animation.getFramesCount();
		return Math.min(nearest, animation.getFramesCount() - 1);
	}

	@Override
	public SpriteData getImage() {
		return animation.getFrame(currentFrame);
	}

}
