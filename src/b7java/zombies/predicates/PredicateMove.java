package b7java.zombies.predicates;

import b7java.zombies.geom.Point;

import b7java.zombies.entities.IEntity;

public abstract class PredicateMove implements IPredicateAction {

	IEntity entity;
	Point prevPoint;
	boolean isCheked;
	
	public PredicateMove(IEntity entity) {
		super();
		this.entity = entity;
	}

	@Override
	public State step(float dtSec) {
		Point p = entity.getPoint();
		if (prevPoint == null){
			isCheked = checkMove(0, 0, 0);
		}
		else {
			isCheked = checkMove(p.x - prevPoint.x, p.y - prevPoint.y, dtSec);
		}
		prevPoint = new Point(p);
		return null;
	}

	@Override
	public boolean is() {
		return isCheked;
	}

	abstract protected boolean checkMove(double dx, double dy, float dtStep);
}
