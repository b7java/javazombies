package b7java.zombies.predicates;

import b7java.zombies.entities.IEntity;
import b7java.zombies.geom.Axis;

public class PredicateMoveByAxis extends PredicateMove {

	Axis axis;
	int direction;
	
	public PredicateMoveByAxis(IEntity entity, Axis axis, int direction) {
		super(entity);
		this.axis = axis;
		this.direction = direction;
	}

	@Override
	protected boolean checkMove(double dx, double dy, float dtStep) {
		double delta = (axis == Axis.X) ? dx : dy;
		return (delta * direction > 0);
	}

}
