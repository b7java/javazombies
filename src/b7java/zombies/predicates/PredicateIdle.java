package b7java.zombies.predicates;

import b7java.zombies.entities.IEntity;

public class PredicateIdle extends PredicateMove {

	public PredicateIdle(IEntity entity) {
		super(entity);
	}

	@Override
	protected boolean checkMove(double dx, double dy, float dtStep) {
		return dx == 0 && dy == 0;
	}

}
