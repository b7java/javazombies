package b7java.zombies.predicates;

import b7java.zombies.actions.IAction;

public interface IPredicateAction extends IAction {
	boolean is();
}
