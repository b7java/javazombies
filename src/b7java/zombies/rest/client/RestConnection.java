package b7java.zombies.rest.client;

import java.util.Collection;

import org.springframework.web.client.RestTemplate;

import b7java.zombies.commands.events.IEvent;
import b7java.zombies.connection.IClient;
import b7java.zombies.connection.IConnection;
import b7java.zombies.entities.IEntity;
import b7java.zombies.rest.api.Defaults;
import b7java.zombies.rest.api.PostEventRequest;
import b7java.zombies.rest.api.PostEventResponse;
import b7java.zombies.rest.api.RequestType;
import b7java.zombies.rest.api.ZombiesResponse;


public class RestConnection implements IConnection {

	static RestTemplate restTemplate = new RestTemplate();
	
	/** current client */
	IClient client;

	public RestConnection(IClient client) {
		super();
		this.client = client;
	}

	@Override
	public Collection<IEntity> getEntities() {
		String url = Defaults.REST_URL + RequestType.GET_ALL_ZOMBIES;
		ZombiesResponse response = restTemplate.getForObject(url, ZombiesResponse.class);
		return response.getZombies();
	}	
	
	
	@Override
	public void sendEvent(IEvent event) {
		event.setClientId(client.getClientId());
		String url = Defaults.REST_URL + RequestType.POST_EVENT;
		restTemplate.postForObject(url, new PostEventRequest(event), PostEventResponse.class);
	}
}
