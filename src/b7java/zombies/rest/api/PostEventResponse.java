package b7java.zombies.rest.api;

import b7java.zombies.entities.IGame;

public class PostEventResponse {

	public PostEventResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	String status = "Ok, event sent!";
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PostEventResponse(IGame game) {
	}

}
