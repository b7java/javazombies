package b7java.zombies.rest.api;

import java.util.ArrayList;

import b7java.zombies.entities.Entity;
import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IGame;

public class ZombiesResponse {

	ArrayList<IEntity> zombies = new ArrayList<>();
	public ArrayList<IEntity> getZombies() {
		return zombies;
	}


	public void setZombies(ArrayList<IEntity> zombies) {
		this.zombies = zombies;
	}


	public ZombiesResponse() {
		super();
		// TODO Auto-generated constructor stub
	}


	public ZombiesResponse(ArrayList<IEntity> zombies, String status) {
		super();
		this.zombies = zombies;
		this.status = status;
	}


	String status = "Empty";
	
	
	public ZombiesResponse(IGame game) {
		if (game == null){
			//zombies.add(new Entity("Jon", 3, 3));
			return;
		}
		game.forEarchEntity(zombies::add);
		status = "Filled";
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}

}
