package b7java.zombies.rest.api;

import b7java.zombies.commands.events.IEvent;

public class PostEventRequest {
	IEvent event;

	public IEvent getEvent() {
		return event;
	}

	public PostEventRequest() {
		super();
	}

	public PostEventRequest(IEvent event) {
		super();
		this.event = event;
	}

	public void setEvent(IEvent event) {
		this.event = event;
	}
}
