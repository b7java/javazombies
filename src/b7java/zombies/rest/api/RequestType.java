package b7java.zombies.rest.api;

public abstract class RequestType {

	public static final String GET_ALL_ZOMBIES = "/zombies";
	public static final String POST_EVENT = "/event";
	public static final String TEST = "/test";

}
