package b7java.zombies.rest.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameRestService {

	public static void main(String[] args) {
		SpringApplication.run(GameRestController.class, args);
	}

}
