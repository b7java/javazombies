package b7java.zombies.rest.server;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.*;
import org.springframework.web.bind.annotation.*;

import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.events.IEvent;
import b7java.zombies.entities.IGame;
import b7java.zombies.rest.api.PostEventRequest;
import b7java.zombies.rest.api.PostEventResponse;
import b7java.zombies.rest.api.RequestType;
import b7java.zombies.rest.api.ZombiesResponse;


@SpringBootApplication
@RestController
@ImportResource("classpath:beans.xml")
public class GameRestController {

	private static IGame game;

	public static IGame getGame() {
		return game;
	}

	public static void setGame(IGame game) {
		GameRestController.game = game;
	}

	public GameRestController() {
	}

	@RequestMapping(value = RequestType.GET_ALL_ZOMBIES, method = RequestMethod.GET)
	public ZombiesResponse getAllZombies() {

		return new ZombiesResponse(game);
	}
	
	@RequestMapping(value = RequestType.TEST, method = RequestMethod.GET)
	public String getTest() {

		return "Hello, world!";
	}



	@RequestMapping(value = RequestType.POST_EVENT, method = RequestMethod.POST)
	public PostEventResponse onEvent(@RequestBody PostEventRequest request) {
		if (game != null){
			IEventProcessor proc = game.getEventProcessor();
			IEvent event = request.getEvent();
			//System.out.println("==============> " + event);
			if (proc != null && event != null){
				proc.onEvent(event);
			}
		}
		return new PostEventResponse(game);
	}
}
