package b7java.zombies.applications;

import b7java.zombies.geom.Point;

import b7java.zombies.actions.RunAction;
import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.view.IView;
import b7java.zombies.view.СontinuousView;
import b7java.zombies.window.GameWindow;

public class App0021_ViewSettings {

	public static void main(String[] args) {
		
		Game game = new Game();

		game.add(new Entity("Zombie", 600, 0));
		Entity plant = new Entity("Plant", 50, 1);
		plant.getSpriteData().imageName = "images/plant.png";
		game.add(plant);
	
		Entity rect = new Entity("Rect", 400, 1);
		rect.getSpriteData().imageName = "images/Tests/Rect.png";
		game.add(rect);
		
		game.add(new RunAction(rect, new Point(400, 500), 3));
		
		GameWindow window = new GameWindow();
		IView view = new СontinuousView(game);
		window.showGame(view);
	}

}
