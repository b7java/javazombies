package b7java.zombies.applications;

import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.connection.Client;
import b7java.zombies.connection.DirectConnection;
import b7java.zombies.connection.IClient;
import b7java.zombies.connection.IConnection;
import b7java.zombies.connection.ReplicatedGame;
import b7java.zombies.connection.SendToServerCommand;
import b7java.zombies.entities.Game;
import b7java.zombies.entities.IGame;
import b7java.zombies.rest.client.RestConnection;
import b7java.zombies.window.GameWindow;

public class App0036_RestClient {

	public static void main(String[] args) throws Exception {
		showDirectReplicaGame("Red");
	}
	/**
	 * Creates a replicated game, using DirectConnection and shows this game
	 * @game - server game
	 * @clientId - client id string
	 */	
	public static void showDirectReplicaGame(String clientId) {
		IClient client = new Client(clientId);
		IConnection connection = new RestConnection(client);
		Game clientGame = new Game();
		IEventProcessor processor = new EventProcessor();
		clientGame.setEventProcessor(processor);		
		processor.addDefaultCommand(new SendToServerCommand(connection));
		
		IGame replica = new ReplicatedGame(clientGame, connection);	
		
		GameWindow window = new GameWindow();
		window.showGame(replica);
		window.setTitle(clientId);
	}
}
