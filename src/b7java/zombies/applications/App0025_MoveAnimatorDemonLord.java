package b7java.zombies.applications;

import b7java.zombies.geom.Point;
import b7java.zombies.resources.DemonLord;
import b7java.zombies.resources.Packman;
import b7java.zombies.actions.AnimationHelper;
import b7java.zombies.actions.RunAction;
import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.window.GameWindow;


public class App0025_MoveAnimatorDemonLord {


	public static void main(String[] args) {

		Game game = new Game();

		addDemon(game, 0, 0, 5);
		addDemon(game, 2, 5, 0);		
		
		
		GameWindow window = new GameWindow();
		window.showGame(game);
	}

	private static void addDemon(Game game, int y, int x0, int x1) {
		Entity demon = new Entity("", x0, y);
		demon.setSpriteData(DemonLord.idleSprite());
		game.add(demon);

		game.add(new RunAction(demon, new Point(x1, y) , 4));
		AnimationHelper.addMoveAnimations(demon, game, 
				DemonLord.moveLeft(), DemonLord.moveRight(),
				  null, null,
				  1);
	}

}