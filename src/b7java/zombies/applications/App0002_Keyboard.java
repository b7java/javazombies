package b7java.zombies.applications;

import java.awt.event.KeyEvent;

import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.ICommand;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.LogGameCommand;
import b7java.zombies.commands.events.KeyboardEventKey;
import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.exceptions.InvalidConfigException;
import b7java.zombies.window.GameWindow;

public class App0002_Keyboard {

	public static void main(String[] args) throws InvalidConfigException {
		Game game = new Game();
		game.add(new Entity("Jon", 7, 0));
		game.add(new Entity("Sam", 6, 1));
		
		ICommand command = new LogGameCommand(game);
		IEventProcessor processor = new EventProcessor();
		processor.add("Log", command);
		processor.bind("Log", new KeyboardEventKey(KeyEvent.VK_SPACE, false, false, false));
		game.setEventProcessor(processor);
		
		GameWindow window = new GameWindow();
		window.showGame(game);

	}

}
