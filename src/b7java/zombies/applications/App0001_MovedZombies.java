package b7java.zombies.applications;

import b7java.zombies.actions.MoveAllAction;
import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.window.GameWindow;

public class App0001_MovedZombies {

	public static void main(String[] args) {
		
		Game game = new Game();
		game.add(new Entity("Jon", 7, 0));
		//game.add(new Entity("Sam", 6, 1));
		
		game.add(new MoveAllAction(1.f, game));
		
		GameWindow window = new GameWindow();
		window.showGame(game);
	}

}
