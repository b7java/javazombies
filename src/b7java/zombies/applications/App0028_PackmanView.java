package b7java.zombies.applications;

import b7java.zombies.entities.Game;
import b7java.zombies.entities.IGame;
import b7java.zombies.images.AnimationFileHelper;
import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.AnimationPlayingSetting.Mode;
import b7java.zombies.resources.Packman;
import b7java.zombies.tilemaps.TileCreatorByAnimation;
import b7java.zombies.tilemaps.TileMapLoader;
import b7java.zombies.tilemaps.TileMapLoadingException;
import b7java.zombies.view.IView;
import b7java.zombies.view.ViewBase;
import b7java.zombies.images.IAnimation;
import b7java.zombies.images.Rect;
import b7java.zombies.window.AnimationViewer;
import b7java.zombies.window.GameWindow;

public class App0028_PackmanView {

	public static void main(String[] args) throws Exception {

		IAnimation animation = Packman.allFrames();
		{
			AnimationPlayingSetting settings = new AnimationPlayingSetting();
			settings.mode = Mode.LOOP;
			settings.dtSec = AnimationPlayingSetting.DEFAULT_STEP * 3;
		
			AnimationViewer.show(animation, settings);
		}

		{
			IGame game = new Game();
			TileCreatorByAnimation tileCreator = new TileCreatorByAnimation(game, animation);
			tileCreator.addTile('=', Packman.frameWall());
			tileCreator.addTile('o', Packman.frameFood());
			tileCreator.addTile('p', Packman.frameMan());
			
			try {
				TileMapLoader.load("tilemaps/packman1.txt", tileCreator);
			} catch (TileMapLoadingException e) {
				e.printStackTrace();
			}
			
			GameWindow window = new GameWindow();
			IView view = new ViewBase(game, Packman.spriteWidth(), Packman.spriteHeight(), 0);
			window.showGame(view);
		}
	}

}
