package b7java.zombies.applications;

import b7java.zombies.actions.AnimationHelper;
import b7java.zombies.actions.ContactEntityToManyAction;
import b7java.zombies.actions.IAction;
import b7java.zombies.actions.persuit.PursuitSimpleAction;
import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.MoveFromClientCommand;
import b7java.zombies.contact.EntityContactManagerIntCoordsDinamic;
import b7java.zombies.contact.EntityContactManagerIntCoordsStatic;
import b7java.zombies.contact.IEntityContactManager;
import b7java.zombies.entities.Game;
import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IText;
import b7java.zombies.entities.Text;
import b7java.zombies.exceptions.InvalidConfigException;
import b7java.zombies.geom.Point;
import b7java.zombies.images.IAnimation;
import b7java.zombies.pass.IPassability;
import b7java.zombies.pass.PassabilityIntWalls;
import b7java.zombies.resources.Packman;
import b7java.zombies.score.IScore;
import b7java.zombies.score.Score;
import b7java.zombies.tilemaps.ILoadedTilesMap;
import b7java.zombies.tilemaps.LoadedTilesMap;
import b7java.zombies.tilemaps.TileCreatorByAnimation;
import b7java.zombies.tilemaps.TileMapLoader;
import b7java.zombies.tilemaps.TileMapLoadingException;
import b7java.zombies.view.DiscreteView;
import b7java.zombies.view.IView;
import b7java.zombies.view.ViewBase;
import b7java.zombies.window.GameWindow;

public class App0041_PackmanGame {

	private static final char TILE_PACKMAN = 'p';
	private static final char TILE_GHOST_RED = 'r';
	private static final char TILE_FOOD = 'o';
	private static final char TILE_WALL = '=';

	
	private static final double MOVE_STEP = 0.1;
	private static final double RED_VELOCITY = 0.8;
	private static final int START_LIFES = 5;
	private static final int LIFES_BOARD_X_OFFSET = 150;

	private static final int SPRITE_DEPTH_PACKMAN = 0;
	private static final int SPRITE_DEPTH_GHOST = 1;
	private static final int SPRITE_DEPTH_FOOD = 2;
	

	public static void main(String[] args) throws Exception {

		Game game = new Game();
		ILoadedTilesMap map = loadTilesMap(game);

		IEntity packman = map.firstOfTile(TILE_PACKMAN);
		// Passability - we can't move through the walls
		IPassability passability = new PassabilityIntWalls(map.getTileObjects(TILE_WALL));
		
		initPackmanSprite(game, packman, passability);
		initFoodSrites(game, map, packman);
		initGhostSprites(game, map, packman, passability);

	
		showGame(game);
	}

	private static void showGame(Game game) {
		GameWindow window = new GameWindow();
		IView view = new ViewBase(game, 0, DiscreteView.DY_TEXT,
								  Packman.spriteWidth(), Packman.spriteHeight(), 0);
		window.showGame(view);
	}

	private static void initPackmanSprite(Game game, IEntity packman, IPassability passability)
			throws InvalidConfigException {
		IEventProcessor processor = new EventProcessor();
		game.setEventProcessor(processor);		
		MoveFromClientCommand.addAllDirections(processor, null, packman, MOVE_STEP, passability);
		AnimationHelper.addMoveAnimations(packman, game, 
										  Packman.moveLeft(), Packman.moveRight(),
										  Packman.moveUp(), Packman.moveDown(),
										  4);
		packman.setDepth(SPRITE_DEPTH_PACKMAN);
	}

	private static ILoadedTilesMap loadTilesMap(Game game) {
		IAnimation animation = Packman.allFrames();
		TileCreatorByAnimation tileCreator = new TileCreatorByAnimation(game, animation);
		tileCreator.addTile(TILE_WALL, Packman.frameWall());
		tileCreator.addTile(TILE_FOOD, Packman.frameFood());
		tileCreator.addTile(TILE_PACKMAN, Packman.frameMan());
		tileCreator.addTile(TILE_GHOST_RED, Packman.frameGhostRed());
		
		ILoadedTilesMap map = new LoadedTilesMap();
		tileCreator.setMap(map);
		
		try {
			TileMapLoader.load("tilemaps/PackmanGhost.txt", tileCreator);
		} catch (TileMapLoadingException e) {
			e.printStackTrace();
		}
		return map;
	}

	private static void initFoodSrites(Game game, ILoadedTilesMap map, IEntity packman) {
		IText board = new Text();
		game.add(board);
		IScore score = new Score(board, "Score: ", 0);
		
		IEntityContactManager food = new EntityContactManagerIntCoordsStatic(game);
		map.getTileObjects(TILE_FOOD).forEach(food::addTargetEntity);
		IAction eating = new ContactEntityToManyAction(food, packman, (f,t)->score.inc(), true);
		game.add(eating);
		
		setSpriteDepth(map, TILE_FOOD, SPRITE_DEPTH_FOOD);
		
	}
	
	private static void initGhostSprites(Game game, ILoadedTilesMap map, IEntity packman, IPassability passability) {
		IText board = new Text();
		board.setPoint(new Point(LIFES_BOARD_X_OFFSET / Packman.spriteWidth(), 0));
		game.add(board);
		IScore score = new Score(board, "Lifes: ", START_LIFES);
		
		Point startPoint = packman.getPoint();
		
		IEntityContactManager ghosts = new EntityContactManagerIntCoordsDinamic(game);
		map.getTileObjects(TILE_GHOST_RED).forEach(ghosts::addTargetEntity);
		IAction eating = new ContactEntityToManyAction(ghosts, packman, 
													   (f,t)->
													   {
														   score.dec(); 
														   if(score.get() == 0){
															   game.gameOver();
														   }
														   else{
															   packman.setPoint(startPoint);
														   }
													   },
													   false);
		game.add(eating);

		PursuitSimpleAction.addActions(map.getTileObjects(TILE_GHOST_RED), game, passability, packman, RED_VELOCITY);
		
		map.getTileObjects(TILE_GHOST_RED).forEach( (ghost)->AnimationHelper.addMoveAnimations(ghost, game, 
				  Packman.ghostLeft(), Packman.ghostRight(),
				  Packman.ghostUp(), Packman.ghostDown(),
				  4));
		
		setSpriteDepth(map, TILE_GHOST_RED, SPRITE_DEPTH_GHOST);
	}	
	
	private static void setSpriteDepth(ILoadedTilesMap map, char tile, int depth){
		map.getTileObjects(tile).forEach(ent->{ent.setDepth(depth);});
	}
}
