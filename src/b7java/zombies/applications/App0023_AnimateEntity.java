package b7java.zombies.applications;

import java.awt.event.KeyEvent;

import b7java.zombies.actions.AnimationAction;
import b7java.zombies.actions.RunAction;
import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.ICommand;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.LogGameCommand;
import b7java.zombies.commands.StartActionCommand;
import b7java.zombies.commands.events.KeyboardEventKey;
import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.exceptions.InvalidConfigException;
import b7java.zombies.images.AnimationFileHelper;
import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.IAnimation;
import b7java.zombies.images.Rect;
import b7java.zombies.window.GameWindow;

public class App0023_AnimateEntity {

	public static void main(String[] args) throws InvalidConfigException {
		Game game = new Game();
		Entity entity = new Entity("Jon", 2, 2);
		game.add(entity);

		AnimationPlayingSetting settings = new AnimationPlayingSetting();
		String preffix = "FW_Demon_Lord_Attack_Flame__";
		String folder = "animations/Demon_Lord";
		IAnimation animation = AnimationFileHelper.animationFromFiles(folder, preffix);
		AnimationAction action = new AnimationAction(entity, animation, settings);
		ICommand command = new StartActionCommand(action, game);
		IEventProcessor processor = new EventProcessor();
		processor.add("Log", command);
		processor.bind("Log", new KeyboardEventKey(KeyEvent.VK_SPACE, false, false, false));
		game.setEventProcessor(processor);
		
		GameWindow window = new GameWindow();
		window.showGame(game);

	}

}
