package b7java.zombies.applications;

import java.awt.event.KeyEvent;

import b7java.zombies.actions.ActionHistoryPlayer;
import b7java.zombies.collision.CollisionDetectorByCoord;
import b7java.zombies.collision.CollisionManager;
import b7java.zombies.collision.CollissionHandlerLog;
import b7java.zombies.collision.ICollisionDetector;
import b7java.zombies.collision.ICollisionManager;
import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.MoveFromClientCommand;
import b7java.zombies.commands.events.Event;
import b7java.zombies.commands.events.IEventKey;
import b7java.zombies.commands.events.KeyboardEventKey;
import b7java.zombies.commands.history.History;
import b7java.zombies.commands.history.HistoryDelegateLog;
import b7java.zombies.commands.history.HistorySaver;
import b7java.zombies.commands.history.IHistoryDelegate;
import b7java.zombies.commands.history.SavedEvent;
import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.exceptions.InvalidConfigException;
import b7java.zombies.images.Rect;
import b7java.zombies.window.GameWindow;

public class App0044_HistorySaver {

	private static final int MOVE_STEP = 1;

	public static void main(String[] args) throws InvalidConfigException {
		Game game = new Game();
		Entity moved = new Entity("Moved", 7, 0);
		game.add(moved);
		
		IEventProcessor processor = new EventProcessor();
		MoveFromClientCommand.addAllDirections(processor, null, moved, MOVE_STEP);
		game.setEventProcessor(processor);
		
		addHistorySaver(game);
		
		GameWindow window = new GameWindow();
		window.showGame(game);

	}

	private static void addHistorySaver(Game game) {
		HistorySaver saver = new HistorySaver();
		saver.setSaveCommandKey(new KeyboardEventKey(KeyEvent.VK_S));
		saver.setSaveDelegate(new HistoryDelegateLog());
		game.getEventProcessor().addDelegate(saver);
		game.add(saver);
	}

}
