package b7java.zombies.applications;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * C:\Users\USERNAME\.aws\config
[default]
region = us-east-2
 */

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.ListTablesRequest;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;

public class App0042_ConnectDynamoDB {

	private static final String ATTRIBUTE_DTO = "DTO";
	private static final String TESTS_KEY = "Id";
	private static final String TABLE_TESTS = "Tests";
	static final AmazonDynamoDB ddb = AmazonDynamoDBClientBuilder.defaultClient();
	static final DynamoDB dynamoDB = new DynamoDB(ddb);
	static final Table table = dynamoDB.getTable(TABLE_TESTS);
	static final  ObjectMapper objectMapper = new ObjectMapper();

    
	public static void main(String[] args) throws JsonProcessingException {
		
		//listTables();
		//addItem();
		//getItem();
		//addItemJSON();
		getItemJSON();
	}

	private static void listTables() {

		ListTablesRequest request;

		boolean more_tables = true;
		String last_name = null;

		while(more_tables) {
		    try {
		        if (last_name == null) {
		        	request = new ListTablesRequest().withLimit(10);
		        }
		        else {
		        	request = new ListTablesRequest()
		        			.withLimit(10)
		        			.withExclusiveStartTableName(last_name);
		        }
		        
		        ListTablesResult table_list = ddb.listTables(request);
		        List<String> table_names = table_list.getTableNames();

		        if (table_names.size() > 0) {
		            for (String cur_name : table_names) {
		                System.out.format("* %s\n", cur_name);
		            }
		        } else {
		            System.out.println("No tables found!");
		            System.exit(0);
		        }

		        last_name = table_list.getLastEvaluatedTableName();
		        if (last_name == null) {
		            more_tables = false;
		        }
		    }
		    catch (Exception ex){
		    	System.out.println(ex);
		    }
		}
	}
	
	static void addItem(){
		int id = (int) Math.round(Math.random() * 100);
        final Map<String, Object> infoMap = new HashMap<String, Object>();
        infoMap.put("plot", "Nothing happens at all.");
        infoMap.put("rating", 0);
			
        try {
	        System.out.println("Adding a new item...");
	        PutItemOutcome outcome = table
	            .putItem(new Item().withPrimaryKey(TESTS_KEY, id).withMap("info", infoMap));
	
	        System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());
        }
        catch (Exception e) {
            System.err.println("Unable to add item");
            System.err.println(e.getMessage());
        }
	}
	
	static class DTORec
	{
		@Override
		public String toString() {
			return "DTORec [index=" + index + "]";
		}

		public DTORec() {
			super();
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		public DTORec(int index) {
			super();
			this.index = index;
		}

		int index = 0;
	}
	static class DTO{
		@Override
		public String toString() {
			return "DTO [name=" + name + ", age=" + age + ", records=" + records + "]";
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		public ArrayList<DTORec> getRecords() {
			return records;
		}
		public void setRecords(ArrayList<DTORec> records) {
			this.records = records;
		}
		public String name = "Sam";
		public int age = 21;
		public ArrayList<DTORec> records = new ArrayList<>();
	}
	
	static void addItemJSON() throws JsonProcessingException{
		int id = 7;
		
		DTO obj = new DTO();
		obj.records.add(new DTORec(7));
		obj.records.add(new DTORec(14));
		
		String json = objectMapper.writeValueAsString(obj);
        
        
		
		
        try {
	        System.out.println("Adding a new item JSON...");
	        
	        Item item = new Item()
	        	    .withPrimaryKey(TESTS_KEY, id)
	        	    .withJSON(ATTRIBUTE_DTO, json);
	        PutItemOutcome outcome = table
	            .putItem(item);
	
	        System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());
        }
        catch (Exception e) {
            System.err.println("Unable to add item");
            System.err.println(e.getMessage());
        }
	}
	
	static void getItem(){
		int id = 96;
		GetItemSpec spec = new GetItemSpec().withPrimaryKey(TESTS_KEY, id);
		
	       try {
	            System.out.println("Attempting to read the item...");
	            Item outcome = table.getItem(spec);
	            System.out.println("GetItem succeeded: " + outcome);

	        }
	        catch (Exception e) {
	            System.err.println("Unable to read item: " + id);
	            System.err.println(e.getMessage());
	        }
	}
	
	static void getItemJSON(){
		int id = 7;
		GetItemSpec spec = new GetItemSpec().withPrimaryKey(TESTS_KEY, id);
		
	       try {
	            System.out.println("Attempting to read the item...");
	            Item outcome = table.getItem(spec);
	            System.out.println("GetItem succeeded: " + outcome);
	            String json = (String) outcome.getJSON(ATTRIBUTE_DTO);
	            DTO obj = objectMapper.readValue(json, DTO.class);
	            System.out.println("DTO:");
	            System.out.println(obj);

	        }
	        catch (Exception e) {
	            System.err.println("Unable to read item: " + id);
	            System.err.println(e.getMessage());
	        }
	}
}
