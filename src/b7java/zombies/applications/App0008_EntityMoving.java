package b7java.zombies.applications;

import b7java.zombies.geom.Point;
import b7java.zombies.actions.RunAction;
import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.window.GameWindow;

//import statements
/**
 * @author Kovalenko Ivan <radjawarior @ mail.ru>
 * @version 1.0
 * @since 1.0
 */
public class App0008_EntityMoving {
	// class body
	public static void main(String[] args) {
		Entity Jon = new Entity("Jon", 8, 0);
		Point end = new Point(0, 8);
		Game game = new Game();
		game.add(Jon);

		game.add(new RunAction(Jon, end, 8));

		GameWindow window = new GameWindow();
		window.showGame(game);
	}

}