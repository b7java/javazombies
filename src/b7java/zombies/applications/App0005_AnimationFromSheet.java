package b7java.zombies.applications;

import b7java.zombies.images.AnimationFileHelper;
import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.AnimationPlayingSetting.Mode;
import b7java.zombies.images.IAnimation;
import b7java.zombies.images.Rect;
import b7java.zombies.window.AnimationViewer;

public class App0005_AnimationFromSheet {

	public static void main(String[] args) throws Exception {

		AnimationPlayingSetting settings = new AnimationPlayingSetting();
		settings.mode = Mode.LOOP;
		
		boolean showDemon = true;
		if (showDemon){
			String imageName = "animations/Demon_Lord_Sheets/FW_Demon_Lord_Running.png";
			IAnimation animation = AnimationFileHelper.animationFromSheet(imageName, 
																		  1482, 1064,
																		  6, 4);
			AnimationViewer.show(animation, settings);
		}	
		else{
			settings.dtSec = AnimationPlayingSetting.DEFAULT_STEP * 3;
			String imageName = "animations/Packman/packman1.png";
			IAnimation animation = AnimationFileHelper.animationFromSheetRow(imageName, 
																		     448, 128,
																		      14, 4,
																		      10, 0, 2);
			AnimationViewer.show(animation, settings);
		}
	}

}
