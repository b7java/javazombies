package b7java.zombies.applications;

import b7java.zombies.actions.AnimationHelper;
import b7java.zombies.actions.ContactEntityToManyAction;
import b7java.zombies.actions.IAction;
import b7java.zombies.actions.persuit.PursuitSimpleAction;
import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.MoveFromClientCommand;
import b7java.zombies.contact.EntityContactManagerIntCoordsStatic;
import b7java.zombies.contact.IEntityContactManager;
import b7java.zombies.entities.Game;
import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IGame;
import b7java.zombies.entities.IText;
import b7java.zombies.entities.Text;
import b7java.zombies.images.AnimationFileHelper;
import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.AnimationPlayingSetting.Mode;
import b7java.zombies.resources.Packman;
import b7java.zombies.score.IScore;
import b7java.zombies.score.Score;
import b7java.zombies.tilemaps.ILoadedTilesMap;
import b7java.zombies.tilemaps.LoadedTilesMap;
import b7java.zombies.tilemaps.TileCreatorByAnimation;
import b7java.zombies.tilemaps.TileMapLoader;
import b7java.zombies.tilemaps.TileMapLoadingException;
import b7java.zombies.view.DiscreteView;
import b7java.zombies.view.IView;
import b7java.zombies.view.ViewBase;
import b7java.zombies.images.IAnimation;
import b7java.zombies.images.Rect;
import b7java.zombies.pass.IPassability;
import b7java.zombies.pass.PassabilityIntWalls;
import b7java.zombies.window.AnimationViewer;
import b7java.zombies.window.GameWindow;

public class App0032_PackmanGhost {

	private static final double MOVE_STEP = 0.1;
	private static final double RED_VELOCITY = 0.9;

	public static void main(String[] args) throws Exception {

		IAnimation animation = Packman.allFrames();
		{
			Game game = new Game();
			TileCreatorByAnimation tileCreator = new TileCreatorByAnimation(game, animation);
			tileCreator.addTile('=', Packman.frameWall());
			tileCreator.addTile('o', Packman.frameFood());
			tileCreator.addTile('p', Packman.frameMan());
			tileCreator.addTile('r', Packman.frameGhostRed());
			
			ILoadedTilesMap map = new LoadedTilesMap();
			tileCreator.setMap(map);
			
			try {
				TileMapLoader.load("tilemaps/PackmanGhost.txt", tileCreator);
			} catch (TileMapLoadingException e) {
				e.printStackTrace();
			}
			
			IEntity packman = map.firstOfTile('p');
			
			IEventProcessor processor = new EventProcessor();
			game.setEventProcessor(processor);		
			IPassability passability = new PassabilityIntWalls(map.getTileObjects('='));
			MoveFromClientCommand.addAllDirections(processor, null, packman, MOVE_STEP, passability);

			AnimationHelper.addMoveAnimations(packman, game, 
											  Packman.moveLeft(), Packman.moveRight(),
											  Packman.moveUp(), Packman.moveDown(),
											  4);
			
			IText board = new Text();
			game.add(board);
			IScore score = new Score(board, "Score: ", 0);
			
			IEntityContactManager food = new EntityContactManagerIntCoordsStatic(game);
			map.getTileObjects('o').forEach(food::addTargetEntity);
			IAction eating = new ContactEntityToManyAction(food, packman, (f,t)->score.inc(), true);
			game.add(eating);
			
			PursuitSimpleAction.addActions(map.getTileObjects('r'), game, passability, packman, RED_VELOCITY);
			
			
			
			GameWindow window = new GameWindow();
			IView view = new ViewBase(game, 0, DiscreteView.DY_TEXT,
									  Packman.spriteWidth(), Packman.spriteHeight(), 0);
			window.showGame(view);
		}
	}	
}
