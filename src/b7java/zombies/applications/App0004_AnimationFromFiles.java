package b7java.zombies.applications;

import b7java.zombies.images.AnimationFileHelper;
import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.IAnimation;
import b7java.zombies.images.Rect;
import b7java.zombies.window.AnimationViewer;

public class App0004_AnimationFromFiles {

	public static void main(String[] args) throws Exception {

		AnimationPlayingSetting settings = new AnimationPlayingSetting();
		String preffix = "FW_Demon_Lord_Attack_Flame__";
		String folder = "animations/Demon_Lord";
		Rect rect = new Rect(100, 100, 50, 50);
		IAnimation animation = AnimationFileHelper.animationFromFiles(folder, preffix, rect);
		//IAnimation animation = AnimationFileHelper.animationFromFiles(folder, preffix);
		AnimationViewer.show(animation, settings);
	}

}
