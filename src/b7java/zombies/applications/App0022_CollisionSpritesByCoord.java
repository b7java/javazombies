package b7java.zombies.applications;

import b7java.zombies.collision.CollisionDetectorByCoord;
import b7java.zombies.collision.CollisionManager;
import b7java.zombies.collision.CollissionHandlerLog;
import b7java.zombies.collision.ICollisionDetector;
import b7java.zombies.collision.ICollisionManager;
import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.MoveFromClientCommand;
import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.exceptions.InvalidConfigException;
import b7java.zombies.images.Rect;
import b7java.zombies.window.GameWindow;

public class App0022_CollisionSpritesByCoord {

	private static final int MOVE_STEP = 1;

	public static void main(String[] args) throws InvalidConfigException {
		Game game = new Game();
		Entity moved = new Entity("Moved", 7, 0);
		Entity plant = new Entity("Stable", 6, 1);
		plant.getSpriteData().imageName = "images/plant.png";
		game.add(moved);
		game.add(plant);
		
		IEventProcessor processor = new EventProcessor();
		MoveFromClientCommand.addAllDirections(processor, null, moved, MOVE_STEP);
		game.setEventProcessor(processor);
		
		ICollisionManager collisionManager = new CollisionManager();
		game.setCollisionManager(collisionManager);
		ICollisionDetector detector = new CollisionDetectorByCoord(CollisionDetectorByCoord.Neighborhood.NEIGHBOURS_4);
		collisionManager.setDetector(detector, false);
		
		CollissionHandlerLog handler = new CollissionHandlerLog(moved, plant);
		collisionManager.addHandler(handler);
		
		GameWindow window = new GameWindow();
		window.showGame(game);

	}

}
