package b7java.zombies.applications;

import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.MoveFromClientCommand;
import b7java.zombies.connection.Client;
import b7java.zombies.connection.DirectConnection;
import b7java.zombies.connection.GameServer;
import b7java.zombies.connection.IClient;
import b7java.zombies.connection.IConnection;
import b7java.zombies.connection.ReplicatedGame;
import b7java.zombies.connection.SendToServerCommand;
import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.entities.IGame;
import b7java.zombies.exceptions.InvalidConfigException;
import b7java.zombies.window.GameWindow;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Tests a client connection: there is one server game and two replicated, 
 * connected to server using DirectConnection
 */
public class App0018_ClientConnection {
	/** Id string for a first client */
	private static final String CLIENT_GREEN = "Green";
	/** Id string for a second client */
	private static final String CLIENT_RED = "Red";
	/** Creature step in X or Y direction when arrow button pressed */	
	private static final int MOVE_STEP = 1;

	public static void main(String[] args) throws InvalidConfigException {
		
		Game game = createServerGame();
		
		GameServer server = new GameServer(game);
		server.start();
		
		showDirectReplicaGame(game, CLIENT_RED);		
		showDirectReplicaGame(game, CLIENT_GREEN);		
	}

	/**
	 * Creates a server game and adds move commands
	 * return - created  game
	 */		
	private static Game createServerGame() throws InvalidConfigException {
		Game game = new Game();
		Entity entityRed = new Entity(CLIENT_RED, 7, 0);
		Entity entityGreen = new Entity(CLIENT_GREEN, 6, 1);
		game.add(entityRed);
		game.add(entityGreen);
		IEventProcessor processor = new EventProcessor();
		game.setEventProcessor(processor);		
		MoveFromClientCommand.addAllDirections(processor, CLIENT_RED, entityRed, MOVE_STEP);
		MoveFromClientCommand.addAllDirections(processor, CLIENT_GREEN, entityGreen, MOVE_STEP);
		return game;
	}

	/**
	 * Creates a replicated game, using DirectConnection and shows this game
	 * @game - server game
	 * @clientId - client id string
	 */	
	private static void showDirectReplicaGame(Game game, String clientId) {
		IClient client = new Client(clientId);
		IConnection connection = new DirectConnection(game, client);
		Game clientGame = new Game();
		IEventProcessor processor = new EventProcessor();
		clientGame.setEventProcessor(processor);		
		processor.addDefaultCommand(new SendToServerCommand(connection));
		
		IGame replica = new ReplicatedGame(clientGame, connection);	
		
		GameWindow window = new GameWindow();
		window.showGame(replica);
		window.setTitle(clientId);
	}
}
