package b7java.zombies.applications;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.MoveFromClientCommand;
import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.exceptions.InvalidConfigException;
import b7java.zombies.rest.server.GameRestController;

@SpringBootApplication
public class App0035_RestServer {
	
	/** Id string for a first client */
	private static final String CLIENT_GREEN = "Green";
	/** Id string for a second client */
	private static final String CLIENT_RED = "Red";
	/** Creature step in X or Y direction when arrow button pressed */	
	private static final int MOVE_STEP = 1;
	
	public static void main(String[] args) throws InvalidConfigException {
		
		Game serverGame = createServerGame();
		GameRestController.setGame(serverGame);
		
		SpringApplication.run(GameRestController.class, args);
	}

	private static Game createServerGame() throws InvalidConfigException {
		Game game = new Game();
		Entity entityRed = new Entity(CLIENT_RED, 3, 0);
		Entity entityGreen = new Entity(CLIENT_GREEN, 6, 1);
		game.add(entityRed);
		game.add(entityGreen);
		IEventProcessor processor = new EventProcessor();
		game.setEventProcessor(processor);		
		MoveFromClientCommand.addAllDirections(processor, CLIENT_RED, entityRed, MOVE_STEP);
		MoveFromClientCommand.addAllDirections(processor, CLIENT_GREEN, entityGreen, MOVE_STEP);
		return game;
	}
}
