package b7java.zombies.applications;

import b7java.zombies.images.AnimationFileHelper;
import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.AnimationPlayingSetting.Mode;
import b7java.zombies.images.IAnimation;
import b7java.zombies.window.AnimationViewer;

public class App0041_PackmanAnimation {

	public static void main(String[] args) throws Exception {

		AnimationPlayingSetting settings = new AnimationPlayingSetting();
		settings.mode = Mode.LOOP;

		settings.dtSec = AnimationPlayingSetting.DEFAULT_STEP * 5;
		String imageName = "animations/Packman/packman1.png";
		IAnimation animation = AnimationFileHelper.animationFromSheetRow(imageName, 
																	     448, 128,
																	      14, 4,
																	      10, 0, 2);
		AnimationViewer.show(animation, settings);

	}

}
