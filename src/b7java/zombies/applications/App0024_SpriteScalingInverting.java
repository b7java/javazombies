package b7java.zombies.applications;

import b7java.zombies.entities.Entity;
import b7java.zombies.entities.Game;
import b7java.zombies.images.Rect;
import b7java.zombies.images.SpriteData;
import b7java.zombies.window.GameWindow;

public class App0024_SpriteScalingInverting {

	public static void main(String[] args) {
		
		Game game = new Game();

		Entity e1 = new Entity("E1", 1, 1);	
		SpriteData s1 = e1.getSpriteData();
		s1.scaleX = -0.5;
		s1.scaleY = -0.5;
		game.add(e1);
		
		Entity e2 = new Entity("E", 0, 0);
		SpriteData s2 = e2.getSpriteData();
		s2.scaleX = -2;
		s2.scaleY = -2;
		s2.rect = new Rect(0, 30, 60, 60);
		game.add(e2);
	
		
		GameWindow window = new GameWindow();
		window.showGame(game);
	}

}
