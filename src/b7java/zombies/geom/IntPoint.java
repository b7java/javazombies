package b7java.zombies.geom;

public class IntPoint {
	
	public int x, y;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntPoint other = (IntPoint) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IntPoint [x=" + x + ", y=" + y + "]";
	}

	public IntPoint(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public IntPoint() {
		super();
	}
	
	public static IntPoint round(Point p){
		return new IntPoint((int)(Math.round(p.x)), (int)(Math.round(p.y)));
	}
	
	public static IntPoint floor(Point p){
		return new IntPoint((int)(Math.floor(p.x)), (int)(Math.floor(p.y)));
	}	
	public static IntPoint ceil(Point p){
		return new IntPoint((int)(Math.ceil(p.x)), (int)(Math.ceil(p.y)));
	}	
}
