package b7java.zombies.geom;

public class Vector {
	public Vector(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public double x;
	public double y;
	
	public void scale(double c){
		x *= c;
		y *= c;
	}
}
