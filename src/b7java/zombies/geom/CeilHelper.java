package b7java.zombies.geom;

import java.util.function.Predicate;

public class CeilHelper {
	
	static public boolean testNeighbours(Point point, Predicate<IntPoint> predicate) {
		IntPoint p0 = IntPoint.floor(point);
		IntPoint p1 = IntPoint.ceil(point);
		IntPoint p = new IntPoint();
		for(p.x = p0.x; p.x <= p1.x; p.x++){
			for(p.y = p0.y; p.y <= p1.y; p.y++){
				if (predicate.test(p)){
					return true;				
				}
			}
		}
		return false;
	}
}
