package b7java.zombies.geom;

public class Point {
	
	public double x = 0;
	public double y = 0;
	
	public Point() {
		super();
	}
	public Point(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	public Point(Point src) {
		super();
		this.x = src.x;
		this.y = src.y;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public void setLocation(double x1, double y1) {
		x = x1;
		y = y1;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}	
	
	public Point moveByVector(Vector v){
		return new Point(x + v.x, y + v.y);
	}
}
