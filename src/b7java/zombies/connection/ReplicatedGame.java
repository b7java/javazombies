package b7java.zombies.connection;

import java.util.Collection;
import java.util.function.Consumer;

import b7java.zombies.actions.IAction;
import b7java.zombies.collision.ICollisionManager;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IGame;
import b7java.zombies.entities.IText;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Game on a client.
 * It gets the own entities and the entities, retrieved from connection
 *
 */

public class ReplicatedGame implements IGame {

	/** Game objects, specific for the given client */
	IGame clientGame;
	
	/** Connection to a server */
	IConnection connection;
	
	/** Entities, retrieved from the server */
	Collection<IEntity> replicatedEntities;
	
	@Override
	public void add(IEntity entity) {
		clientGame.add(entity);	
	}

	@Override
	public void remove(IEntity entity) {
		clientGame.remove(entity);	
	}

	@Override
	public void add(IAction action) {
		clientGame.add(action);	
	}

	@Override
	public void step(float dtSec) {
		fromConnection();
		clientGame.step(dtSec);
	}

	@Override
	public IEventProcessor getEventProcessor() {
		return clientGame.getEventProcessor();
	}
	
	/** retrieves entities from server */
	void fromConnection(){
		replicatedEntities = connection.getEntities();
	}
	
	public ReplicatedGame(IGame clientGame, IConnection connection) {
		super();
		this.clientGame = clientGame;
		this.connection = connection;
	}
	
	@Override
	public void forEarchEntity(Consumer<IEntity> consumer) {
		if(replicatedEntities != null){
			replicatedEntities.forEach(consumer);
		}
		clientGame.forEarchEntity(consumer);
		
	}

	@Override
	public ICollisionManager getCollisionManager() {
		return clientGame.getCollisionManager();
	}

	@Override
	public void forEarchText(Consumer<IText> consumer) {
		clientGame.forEarchText(consumer);
	}

	@Override
	public void add(IText text) {
		clientGame.add(text);
		
	}

	@Override
	public void remove(IText text) {
		clientGame.remove(text);
	}

	@Override
	public void gameOver() {
		clientGame.gameOver();
	}

	@Override
	public boolean isGameOver() {
		return clientGame.isGameOver();
	}
}