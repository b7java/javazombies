package b7java.zombies.connection;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * IClient implementation
 *
 */

public class Client implements IClient {

	@Override
	public String getClientId() {
		return clientId;
	}
	
	public Client(String clientId) {
		super();
		this.clientId = clientId;
	}

	/** Client id */
	String clientId;

}
