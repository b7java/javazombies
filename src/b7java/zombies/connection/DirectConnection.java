package b7java.zombies.connection;

import java.util.ArrayList;
import java.util.Collection;

import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.events.IEvent;
import b7java.zombies.entities.Game;
import b7java.zombies.entities.IEntity;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * IConnection implementation, which connects with server game directly, without web service 
 *
 */

public class DirectConnection implements IConnection {

	/** server game */
	Game serverGame;
	
	/** current client */
	IClient client;

	public DirectConnection(Game serverGame, IClient client) {
		super();
		this.serverGame = serverGame;
		this.client = client;
	}

	@Override
	public Collection<IEntity> getEntities() {
		ArrayList<IEntity> entities = new ArrayList<>();
		serverGame.forEarchEntity(entities::add);
		return entities;
	}	
	
	
	@Override
	public void sendEvent(IEvent event) {
		IEventProcessor processor = serverGame.getEventProcessor();
		if(processor != null){
			event.setClientId(client.getClientId());
			processor.onEvent(event);
		}
	}
}
