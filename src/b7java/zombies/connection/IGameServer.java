package b7java.zombies.connection;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Ticks the game by timer ...
 *
 */

public interface IGameServer {
	
	/** starts the game ticking */
	void start();
}
