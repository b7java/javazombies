package b7java.zombies.connection;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Client information
 *
 */

public interface IClient {

	/** returns id string*/
	String getClientId();
	
	/** 
	 * compares id string
	 * @return true, if both ids are null or both not null and they are equals
	 */	
	
	static boolean compareIDs(String id1, String id2){
		if (id1 == id2){
			return true;
		}
		return id1 != null && id1.equals(id2);
	}
}
