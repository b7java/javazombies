package b7java.zombies.connection;

import java.util.Timer;
import java.util.TimerTask;

import b7java.zombies.entities.IGame;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * IGameServer implementation, ticks the game by timer
 *
 */

public class GameServer implements IGameServer{
	
	/** Tick time step, msec */
	private static final int TIME_STEP = 30;
	
	/** Game */
	IGame game;
	
	/** Timer */	
	Timer timer;
	
	/** Timer task*/		
	GameTimerTask task = new GameTimerTask(this);

	public IGame getGame() {
		return game;
	}

	public GameServer(IGame game) {
		super();
		this.game = game;
	}
	
	/** Task for timer, wich calls gameServer.step();*/	
	public class GameTimerTask extends TimerTask {

		public GameTimerTask(GameServer gameServer) {
			super();
			this.gameServer = gameServer;
		}
		@Override
		public void run() {
			gameServer.step();
		}
		GameServer gameServer;
	}
	
	@Override
	public void start()
	{
        timer = new Timer();
        timer.schedule(task, TIME_STEP, TIME_STEP);
	}	
	
	/** Timer step */
	public void step()
	{
		game.step(TIME_STEP * 1.E-3f);
	}	
}
