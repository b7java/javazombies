package b7java.zombies.connection;

import b7java.zombies.commands.ICommand;
import b7java.zombies.commands.events.IEvent;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * 	Sends event to the server thought connection
 */

public class SendToServerCommand implements ICommand {

	@Override
	public void run(IEvent event) {
		connection.sendEvent(event);
	}
	/** Connection */
	IConnection connection;
	
	public SendToServerCommand(IConnection connection) {
		super();
		this.connection = connection;
	}
}
