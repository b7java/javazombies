package b7java.zombies.connection;

import java.util.Collection;

import b7java.zombies.commands.events.IEvent;
import b7java.zombies.entities.IEntity;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Retrieves information from server
 * and sends events to it.
 */

public interface IConnection {
	
	/**
	 * @return received entities	
	 */
	Collection<IEntity> getEntities();
	
	/**
	 * @param event sent event	
	 */
	void sendEvent(IEvent event);	
}