package b7java.zombies.collision;

import java.util.HashMap;
import java.util.Map;
import javafx.util.Pair;

import b7java.zombies.entities.Entity;
import b7java.zombies.entities.IEntity;
import b7java.zombies.collision.ICollisionDetector;

public class CollisionDetectorWithCache implements ICollisionDetector {
	Map<Pair<IEntity, IEntity>, Boolean> cache = new HashMap<>();
	ICollisionDetector detector;
	
	public CollisionDetectorWithCache(ICollisionDetector detector) {
		super();
		this.detector = detector;
	}

	@Override
	public boolean areCollide(IEntity first, IEntity second) {
		Pair<IEntity, IEntity> key = new Pair<IEntity, IEntity>(first, second);
		Boolean res = cache.get(key);
		if (res != null)
			return res;
		
		boolean collides = detector.areCollide(first, second);
		
		cache.put(key, collides);
		Pair<IEntity, IEntity> inverted = new Pair<IEntity, IEntity>(second, first);
		cache.put(inverted, collides);
		
		return collides;
	}

	@Override
	public void clearCache() {
		cache.clear();
	}


}
