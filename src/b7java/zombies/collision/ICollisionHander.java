package b7java.zombies.collision;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Handles sprite collisions.
 */
@FunctionalInterface
public interface ICollisionHander {
	void process(ICollisionDetector detector);
}
