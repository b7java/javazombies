package b7java.zombies.collision;

import b7java.zombies.entities.IEntity;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Checks if two entities collide.
 */
public interface ICollisionDetector {
	/** Returns true if entities collide*/
	boolean areCollide(IEntity first, IEntity second);
	
	void clearCache();
}
