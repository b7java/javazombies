package b7java.zombies.collision;

import b7java.zombies.entities.IEntity;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Handles sprite collisions.
 */
public class CollissionHandlerLog implements ICollisionHander {

	IEntity first, second;
	boolean prevValue = false;
	
	public CollissionHandlerLog(IEntity first, IEntity second) {
		super();
		this.first = first;
		this.second = second;
	}

	@Override
	public void process(ICollisionDetector detector) {
		boolean value = detector.areCollide(first, second);
		if (value != prevValue){
			log(value);
			prevValue = value;
		}

	}

	private void log(boolean value) {
		System.out.println("Collision " +
							first.getName() + " and " + second.getName() + 
							" = " + value);
	}

}
