package b7java.zombies.collision;

import b7java.zombies.geom.Point;

import b7java.zombies.entities.IEntity;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * Checks if two entities coordinates are neighbor.
 */
public class CollisionDetectorByCoord implements ICollisionDetector {

	public enum Neighborhood{
		EQ,
		NEIGHBOURS_4,
		NEIGHBOURS_8
	}

	Neighborhood neighborhood;	
	
	@Override
	public boolean areCollide(IEntity first, IEntity second) {
		Point p1 = first.getPoint();
		Point p2 = second.getPoint();
		long dx = Math.abs(Math.round(p1.x) - Math.round(p2.x));
		long dy = Math.abs(Math.round(p1.y) - Math.round(p2.y));
		if (dx > 1 || dy > 1 ){
			return false;
		}
		
		if (neighborhood ==Neighborhood.EQ && (dx > 0 || dy > 0))
			return false;
		
		if (dx == 1 && dy == 1){
			return neighborhood == Neighborhood.NEIGHBOURS_8;
		}
		
		return true;
	}

	public CollisionDetectorByCoord(Neighborhood neighborhood) {
		super();
		this.neighborhood = neighborhood;
	}

	@Override
	public void clearCache() {
	
	}


}
