package b7java.zombies.collision;

public interface ICollisionManager {

	void addHandler(ICollisionHander handler);
	void removeHandler(ICollisionHander handler);
	
	void setDetector(ICollisionDetector detector, boolean useCache);
	
	void step();
}
