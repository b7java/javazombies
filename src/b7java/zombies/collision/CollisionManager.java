package b7java.zombies.collision;

import java.util.HashSet;
import java.util.Set;

public class CollisionManager implements ICollisionManager {

	@Override
	public void addHandler(ICollisionHander handler) {
		handlers.add(handler);
	}

	@Override
	public void removeHandler(ICollisionHander handler) {
		handlers.remove(handler);
	}

	@Override
	public void setDetector(ICollisionDetector detector, boolean useCache) {
		if (!useCache){
			this.detector = detector; 
		}
		else{
			this.detector = new CollisionDetectorWithCache(detector);
		}

	}

	@Override
	public void step() {
		if (detector == null){
			return;
		}
		for(ICollisionHander c : handlers){
			c.process(detector);
		}
		detector.clearCache();
	}

	Set<ICollisionHander> handlers = new HashSet<>();
	ICollisionDetector    detector;
}
