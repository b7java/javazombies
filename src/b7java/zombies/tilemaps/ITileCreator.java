package b7java.zombies.tilemaps;

public interface ITileCreator {
	boolean create(int x, int y, char ch);
	void setMap(ILoadedTilesMap map);
}
