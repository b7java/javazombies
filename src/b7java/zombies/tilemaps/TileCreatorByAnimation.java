package b7java.zombies.tilemaps;

import java.util.HashMap;
import java.util.Map;

import b7java.zombies.entities.Entity;
import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IGame;
import b7java.zombies.images.IAnimation;

public class TileCreatorByAnimation implements ITileCreator {
	IGame game;
	IAnimation animation;
	Map<Character, Integer> char2Frame = new HashMap<>();
	ILoadedTilesMap map;
	
	@Override
	public boolean create(int x, int y, char ch) {
		Integer frame = char2Frame.get(ch);
		if (frame == null || frame < 0 || frame >= animation.getFramesCount())
			return false;
		
		IEntity entity = new Entity("" + ch, x, y);
		entity.setSpriteData(animation.getFrame(frame));
		game.add(entity);
		if (map != null){
			map.add(ch, entity);
		}
		return true;
	}

	public TileCreatorByAnimation(IGame game, IAnimation animation) {
		super();
		this.game = game;
		this.animation = animation;
	}
	
	public void addTile(char ch, int frameNumber){
		char2Frame.put(ch, frameNumber);
	}

	@Override
	public void setMap(ILoadedTilesMap map) {
		this.map = map;
	}
}
