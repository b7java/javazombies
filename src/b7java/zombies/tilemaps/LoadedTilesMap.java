package b7java.zombies.tilemaps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import b7java.zombies.entities.IEntity;

public class LoadedTilesMap implements ILoadedTilesMap {

	Map<Character, List<IEntity>> map = new HashMap<>();
	
	@Override
	public void add(char tile, IEntity entity) {
		List<IEntity> list = map.get(tile);
		if (list == null){
			list = new ArrayList<>();
			map.put(tile, list);
		}
		list.add(entity);
	}

	@Override
	public List<IEntity> getTileObjects(char tile) {
		List<IEntity> list = map.get(tile);
		if (list == null){
			list = new ArrayList<>();
		}
		return list;
	}

}
