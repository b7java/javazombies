package b7java.zombies.tilemaps;

import java.util.List;

import b7java.zombies.entities.IEntity;

public interface ILoadedTilesMap {
	void add(char tile, IEntity entity);
	List<IEntity> getTileObjects(char tile);
	
	default IEntity firstOfTile(char tile){
		List<IEntity> list = getTileObjects(tile);
		return list != null && !list.isEmpty() ? list.get(0) : null;
	}
}
