package b7java.zombies.tilemaps;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TileMapLoader {
	public static boolean load(String fileName, ITileCreator tileCreator) throws TileMapLoadingException{
		try(Scanner scanner = new Scanner(new File(fileName)))
		{
			if (!scanner.hasNext())
				throw new TileMapLoadingException("Empty file", fileName);
			
			for(int y = 0; scanner.hasNextLine(); y++){
				String line = scanner.nextLine();
				loadLine(line, y, fileName, tileCreator);
			}
		} catch (FileNotFoundException e) {
			throw new TileMapLoadingException("File not found", fileName);
		}		
		return true;
	}

	private static void loadLine(String line, int y, 
								 String fileName, ITileCreator tileCreator)  
								 throws TileMapLoadingException {
		for(int x = 0; x < line.length(); x++){
			char ch = line.charAt(x);
			if (ch == ' ')
				continue;
			if (!tileCreator.create(x, y, ch))
				throw new TileMapLoadingException("Invalid tile " + ch + " (" + x + ", " + y + ")", fileName);
		}
	}
}
