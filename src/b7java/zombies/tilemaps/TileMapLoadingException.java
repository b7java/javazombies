package b7java.zombies.tilemaps;

public class TileMapLoadingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6882040514055454089L;
	public TileMapLoadingException(String message, String fileName) {
		super(message);
		this.fileName = fileName;
	}
	public final String getFileName() {
		return fileName;
	}
	String fileName;
}
