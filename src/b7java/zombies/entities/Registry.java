package b7java.zombies.entities;

import java.util.HashMap;
import java.util.Map;

public class Registry implements IRegistry {

	public Registry() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void set(String key, Object value) {
		parameters.put(key, value);
	}

	@Override
	public Object get(String key) {
		return parameters.get(key);
	}

	Map<String, Object> parameters = new HashMap<>();
}
