package b7java.zombies.entities;

import java.util.ArrayList;
import java.util.function.Consumer;

import b7java.zombies.actions.ActionList;
import b7java.zombies.actions.IAction;
import b7java.zombies.collision.ICollisionManager;
import b7java.zombies.commands.IEventProcessor;

public class Game implements IGame {

	@Override
	public void forEarchEntity(Consumer<IEntity> consumer) {
		allEntities.forEach(consumer);
	}

	ArrayList<IEntity> allEntities = new ArrayList<>();
	ArrayList<IText> allTexts = new ArrayList<>();
	boolean isGameOver = false;

	@Override
	public void add(IEntity entity) {
		allEntities.add(entity);
		
	}

	@Override
	public void remove(IEntity entity) {
		allEntities.remove(entity);
	}

	@Override
	public void step(float dtSec) {
		if(isGameOver){
			return;
		}
		actionList.step(dtSec);
		if (collisionManager != null){
			collisionManager.step();
		}
	}
	
	ActionList actionList = new ActionList();

	@Override
	public void add(IAction action) {
		actionList.add(action);
	}

	@Override
	public IEventProcessor getEventProcessor() {
		return processor;
	}	
	
	public final void setEventProcessor(IEventProcessor processor) {
		this.processor = processor;
	}

	IEventProcessor processor;
	ICollisionManager collisionManager;

	@Override
	public ICollisionManager getCollisionManager() {
		return collisionManager;
	}
	
	public final void setCollisionManager(ICollisionManager manager) {
		this.collisionManager = manager;
	}

	@Override
	public void forEarchText(Consumer<IText> consumer) {
		allTexts.forEach(consumer);
		
	}

	@Override
	public void add(IText text) {
		allTexts.add(text);
		
	}

	@Override
	public void remove(IText text) {
		allTexts.remove(text);
	}

	@Override
	public void gameOver() {
		isGameOver = true;
	}
	
	@Override
	public boolean isGameOver() {
		return isGameOver;
	}	
}
