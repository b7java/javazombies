package b7java.zombies.entities;

/**
 * Stores values of different types
 */
public interface IRegistry {
	/**
	 * Sets a value. If a value with this key exists, it replaced
	 * @param key a key
	 * @param value new value for a key
	 */
	void set(String key, Object value);
	
	/**
	 * Gets a value for specified key
	 * @param key a key
	 */
	Object get(String key);	
	
	/**
	 * Gets a value for specified key of a given class, returns null if a class mismatch
	 * @param key a key
	 */	
	@SuppressWarnings("unchecked")
	default <T> T get(String key, Class<T> clazz){
		Object obj = get(key);
		if (obj == null){
			return null;
		}
		
		if (clazz.isInstance(obj))
			return (T) obj;
		return null;
	}
	
	/**
	 * Gets a string value for specified keys
	 * @param key a key
	 */	
	default String getString(String key){
		Object obj = get(key);
		if (obj == null){
			return null;
		}
		return obj.toString();
	}	
}
