package b7java.zombies.entities;

import b7java.zombies.geom.Point;


public interface IText {
	String getText();	
	Point getPoint();

	void setText(String text);	
	void setPoint(Point point);
}
