package b7java.zombies.entities;

import b7java.zombies.geom.Point;

import b7java.zombies.images.SpriteData;

public class Entity implements IEntity {

	public Entity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Entity(String name, Point point) {
		super();
		this.name = name;
		this.point = point;
	}
//	
//	public IRegistry getRegistry() {
//		return registry;
//	}
//
//	public void setRegistry(IRegistry registry) {
//		this.registry = registry;
//	}

	public Entity(String name, int x, int y) {
		super();
		this.name = name;
		this.point = new Point(x, y);
	}
	
	String name;
	Point  point;
	int    depth;
//	IRegistry registry = new Registry();
	SpriteData spriteData = null;
	
	@Override
	public Point getPoint() {
		return point;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;	
	}

	@Override
	public void setPoint(Point point) {
		this.point = point;
	}

	@Override
	public SpriteData getSpriteData() {
		if (spriteData == null){
			spriteData = new SpriteData();
		}
		return spriteData;
	}

	@Override
	public void setSpriteData(SpriteData spriteData) {
		this.spriteData = spriteData;
	}

	@Override
	public int getDepth() {
		return depth;
	}

	@Override
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	
}
