package b7java.zombies.entities;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import b7java.zombies.geom.Point;

import b7java.zombies.images.SpriteData;

/**
 * Entity of a game
 */
@JsonTypeInfo(use=Id.CLASS)
public interface IEntity {
	String getName();	
	Point getPoint();
	int   getDepth();
	SpriteData getSpriteData();

	void setName(String name);	
	void setPoint(Point point);
	void setDepth(int depth);
	void setSpriteData(SpriteData spriteData);
	
	/**
	 * IRegistry with parameters
	 */	
	//IRegistry registry();
}
