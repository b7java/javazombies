package b7java.zombies.entities;

import java.util.function.Consumer;

import b7java.zombies.actions.IAction;
import b7java.zombies.collision.ICollisionManager;
import b7java.zombies.commands.IEventProcessor;

public interface IGame {

	void forEarchEntity(Consumer<IEntity> consumer);
	
	void add(IEntity entity);
	void remove(IEntity entity);
	
	void add(IAction action);
	
	void step(float dtSec);
	
	IEventProcessor getEventProcessor();
	
	ICollisionManager getCollisionManager();

	void forEarchText(Consumer<IText> consumer);
	
	void add(IText text);
	void remove(IText text);
	
	void gameOver();
	boolean isGameOver();
}
