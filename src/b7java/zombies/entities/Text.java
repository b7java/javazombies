package b7java.zombies.entities;

import b7java.zombies.geom.Point;

public class Text implements IText {

	String text = null;
	Point point = new Point();
	
	@Override
	public String getText() {
		return text;
	}

	@Override
	public Point getPoint() {
		return point;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}

	public Text() {
		super();
	}

	@Override
	public void setPoint(Point point) {
		this.point = point;
	}
	
	public Text(String text, Point point) {
		super();
		this.text = text;
		this.point = point;
	}
}
