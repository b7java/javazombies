package b7java.zombies.images;

import java.io.File;
import java.util.Arrays;

public class AnimationFileHelper {
	
	static final String [] EXTENSIONS = {".png", ".jpg"};
	
	static public IAnimation animationFromFiles(String folder, String preffix){
		return 	animationFromFiles(folder, preffix, null);
	}

	static public IAnimation animationFromFiles(String folder, String preffix, Rect rect){
			File dir = new File(folder);
			String [] fileNames = dir.list(
					(fdir,name)->{
						return checkPreffix(name, preffix) && checkExt(name);
					});
			Arrays.sort(fileNames);
			for (int i = 0; i < fileNames.length; i++){
				fileNames[i] = folder + '/' +fileNames[i];
			}
			return new AnimationFromFiles(fileNames, rect);
	}
	
	private static boolean checkPreffix(String name, String preffix) {
		return preffix == null || name.startsWith(preffix);
	}

	private static boolean checkExt(String name) {
		for (String ext : EXTENSIONS){
			if( name.endsWith(ext)){
				return true;
			}
		}
		return false;
	}
	
	public static IAnimation animationFromSheet(String imageName, int width, int height,
			  int columns, int rows){
		return animationFromSheet(imageName, width, height, columns, rows, columns * rows);
	}

	public static IAnimation animationFromSheet(String imageName, int width, int height,
								  int columns, int rows, int nFrames){
		AnimationFromSheet animation = new AnimationFromSheet();
		animation.setImageName(imageName);
		int w = width / columns;
		int h = height / rows;
		int frame = 0;
		for(int j = 0; j < rows && frame < nFrames; j++){
			for (int i = 0; i < columns && frame < nFrames; i++){
				Rect rect = new Rect(i * w, j * h, w, h);
				animation.addRect(rect);
			}
		}
		return animation;
	}
	
	public static IAnimation animationFromSheetColumn(String imageName, int width, int height,
				int columns, int rows, 
				int startColumn, int startRow,
				int nFrames){
		return animationFromSheetLine(imageName, width, height,
									  columns, rows, 
									  startColumn, startRow,
									  0, 1, nFrames);
	}
	
	public static IAnimation animationFromSheetRow(String imageName, int width, int height,
			int columns, int rows, 
			int startColumn, int startRow,
			int nFrames){
	return animationFromSheetLine(imageName, width, height,
								  columns, rows, 
								  startColumn, startRow,
								  1, 0, nFrames);
	}	
	
	public static IAnimation animationFromSheetLine(String imageName, int width, int height,
			  										int columns, int rows, 
			  										int startColumn, int startRow,
			  										int stepColumn, int stepRow, int nFrames){
		AnimationFromSheet animation = new AnimationFromSheet();
		animation.setImageName(imageName);
		int w = width / columns;
		int h = height / rows;
		for(int frame = 0; frame < nFrames; frame++){
			int i = startColumn + frame * stepColumn;
			int j = startRow + frame * startRow;
			Rect rect = new Rect(i * w, j * h, w, h);
			animation.addRect(rect);
		}
		return animation;
	}

	public static IAnimation animationFromSheetFrames(IAnimation allFramesAnimation,
													  int [] frames){
		AnimationFromSheet animation = new AnimationFromSheet();
		for (int i = 0; i < frames.length; i++){
			SpriteData sd = allFramesAnimation.getFrame(frames[i]);
			if (i == 0){
				animation.setImageName(sd.imageName);
			}
			animation.addRect(sd.rect);
		}
		return animation;
	}
	
}
