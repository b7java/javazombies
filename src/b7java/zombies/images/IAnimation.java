package b7java.zombies.images;

public interface IAnimation {
	int getFramesCount();
	SpriteData getFrame(int frame);
}
