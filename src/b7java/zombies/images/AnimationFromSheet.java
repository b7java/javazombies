package b7java.zombies.images;

import java.util.ArrayList;
import java.util.List;

public class AnimationFromSheet implements IAnimation {

	String imageName;
	@Override
	public String toString() {
		return "AnimationFromSheet [imageName=" + imageName + ", rects=" + rects + "]";
	}

	List<Rect> rects = new ArrayList<>();
	
	@Override
	public int getFramesCount() {
		return rects.size();
	}

	@Override
	public SpriteData getFrame(int frame) {
		SpriteData data = new SpriteData(imageName, rects.get(frame));
		return data;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public List<Rect> getRects() {
		return rects;
	}

	public void setRects(List<Rect> rects) {
		this.rects = rects;
	}
	
	public void addRect(Rect rect){
		rects.add(rect);
	}

	public AnimationFromSheet(String imageName, List<Rect> rects) {
		super();
		this.imageName = imageName;
		this.rects = rects;
	}

	public AnimationFromSheet() {
		super();
	}

}
