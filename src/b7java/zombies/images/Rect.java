package b7java.zombies.images;

public class Rect {
	public Rect(int x, int y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public Rect() {
		super();
		this.x = 0;
		this.y = 0;
		this.width = 0;
		this.height = 0;
	}
	
	@Override
	public String toString() {
		return "Rect [x=" + x + ", y=" + y + ", width=" + width + ", height=" + height + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		Rect r = (Rect) obj;
		return x == r.x && y == r.y && width == r.width && height == r.height;
	}

	public int x, y, width, height;
}
