package b7java.zombies.images;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AnimationFromFiles implements IAnimation {

	@Override
	public int getFramesCount() {
		return imageNames.size();
	}

	@Override
	public SpriteData getFrame(int frame) {
		return new SpriteData(imageNames.get(frame), rect);
	}

	public AnimationFromFiles() {
		super();
		imageNames = new ArrayList<>();
	}
	
	public AnimationFromFiles(String [] imageNames, Rect rect) {
		super();
		this.imageNames = Arrays.asList(imageNames);
		this.rect = rect;
	}
	
	@Override
	public String toString() {
		return "AnimationFromFiles [imageNames=" + imageNames + "]";
	}

	List<String> imageNames;
	Rect rect;
	public Rect getRect() {
		return rect;
	}

	public void setRect(Rect rect) {
		this.rect = rect;
	}
}
