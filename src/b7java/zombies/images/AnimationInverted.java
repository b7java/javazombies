package b7java.zombies.images;

import b7java.zombies.geom.Axis;

public class AnimationInverted implements IAnimation {

	public AnimationInverted(IAnimation animation, Axis axis) {
		super();
		this.animation = animation;
		this.axis = axis;
	}

	IAnimation animation;
	Axis axis;
	
	@Override
	public int getFramesCount() {
		return animation.getFramesCount();
	}

	@Override
	public SpriteData getFrame(int frame) {
		SpriteData data = new SpriteData(animation.getFrame(frame));
		if (axis == Axis.X){
			data.scaleX *= -1;
		}
		else{
			data.scaleY *= -1;		
		}
		return data;
	}

}
