package b7java.zombies.images;

public class SpriteData {
	
	public String imageName = null;
	public Rect rect = null;
	public double scaleX = 1, scaleY = 1; 
	
	public SpriteData(String imageName, Rect rect) {
		super();
		this.imageName = imageName;
		this.rect = rect;
	}
	
	public SpriteData(String imageName) {
		super();
		this.imageName = imageName;
	}
	
	public SpriteData(SpriteData src) {
		super();
		this.imageName = src.imageName;
		this.rect = src.rect;
		this.scaleX = src.scaleX;
		this.scaleY = src.scaleY;
	}

	public SpriteData() {
	}	
	
}
