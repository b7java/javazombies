package b7java.zombies.images;

public class AnimationPlayingSetting {
	public AnimationPlayingSetting() {
		super();
	}	
	public AnimationPlayingSetting(Mode mode) {
		super();
		this.mode = mode;
	}
	public static final float DEFAULT_STEP = 0.033f;
	public enum Mode{
		LOOP,
		FIX_IN_END_FRAME
	}; 
	
	public Mode mode = Mode.FIX_IN_END_FRAME;
	public float dtSec = DEFAULT_STEP;
}
