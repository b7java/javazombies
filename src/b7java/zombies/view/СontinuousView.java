package b7java.zombies.view;

import b7java.zombies.entities.IGame;

public class СontinuousView extends ViewBase {

	private static final int DX = 1;
	private static final int DY = 1;
	private static final int DY_TEXT = 0;
	public СontinuousView(IGame game) {
		super(game, DX, DY, DY_TEXT);
	}
}
