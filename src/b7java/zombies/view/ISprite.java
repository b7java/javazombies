package b7java.zombies.view;

import java.awt.Graphics2D;

public interface ISprite {
	void paint(Graphics2D g2d);
}