package b7java.zombies.view;

import b7java.zombies.geom.Point;

public interface IPointTransform {
	Point transform(Point p);
}
