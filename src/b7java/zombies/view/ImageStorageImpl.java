package b7java.zombies.view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;


public class ImageStorageImpl implements IImageStorage {

	private static final String DEFAULT_IMAGE_NAME = "images/zombie.png";
	private static final String IMAGE_ROOT = "";
    String defaultImageName = DEFAULT_IMAGE_NAME;
	BufferedImage defaultImage;
	Map<String, BufferedImage> imageMap = new HashMap<>();
	String imageRoot = IMAGE_ROOT;
	
	public ImageStorageImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ImageStorageImpl(String imageRoot) {
		super();
		this.imageRoot = imageRoot;
	}


	@Override
	public BufferedImage image(String imageName) {
		
		if (imageName == null || imageName.isEmpty()){
			if (defaultImage == null)
				defaultImage = createImage(defaultImageName);
			return defaultImage;			
		}
		
		if(imageMap.containsKey(imageName)){
			return imageMap.get(imageName);
		}

		BufferedImage image = createImage(imageName);
		imageMap.put(imageName, image);
		return image;
	}
	
	BufferedImage createImage(String imageName)
	{
		String path = imageRoot + imageName;
		File imageSrc = new File(path);
		try {
			BufferedImage image = ImageIO.read(imageSrc);
			image = setTransparence(image);
			return image;
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return null;
	}
	
    private static BufferedImage setTransparence(BufferedImage image) {
        final int w = image.getWidth();
        final int h = image.getHeight();
        int[] pixels = new int[w * h];
        image.getRGB(0, 0, w, h, pixels, 0, w);
      
        int transparentPixel = pixels[0];
        final int destAlpha = 0;
        
        for (int i = 0; i < pixels.length; i++) {
        	if (pixels[i] == transparentPixel)
        		pixels[i] = destAlpha | (pixels[i] & 0xFFFFFF);
        }

        image.setRGB(0, 0, w, h, pixels, 0, w);
        return image; 
    }
	
	public String getDefaultImageName() {
		return defaultImageName;
	}

	public void setDefaultImageName(String defaultImageName) {
		this.defaultImageName = defaultImageName;
	}
	
}
