package b7java.zombies.view;

import b7java.zombies.entities.IGame;

public class DiscreteView extends ViewBase {

	private static final int DX = 95;
	private static final int DY = 190;
	public static final int DY_TEXT = 20;
	public DiscreteView(IGame game) {
		super(game, DX, DY, DY_TEXT);
	}
}
