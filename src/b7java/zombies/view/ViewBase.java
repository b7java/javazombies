package b7java.zombies.view;

import java.util.ArrayList;
import java.util.function.Consumer;

import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IGame;

public class ViewBase implements IView {

	private static final int X0 = 10;
	private static final int Y0 = 10;

	private static final int DEF_WIDTH =  8 * 95;
	private static final int DEF_HEIGHT = 4 * 210;	

	public ViewBase(IGame game, int dx, int dy, int dyText) {
		this(game, 0, 0, dx, dy, dyText);
//		this.game = game;
//		imageTransform = new PointTransform(X0, Y0 + dyText,     dx, dy + dyText);
//		textTransform  = new PointTransform(X0, Y0 + dyText - 2, dx, dy + dyText);	
//		defWidth = DEF_WIDTH;
//		defHeight =  DEF_HEIGHT;
//		
//		isShowTitles = dyText > 0;
	}
	
	public ViewBase(IGame game, int x0, int y0, int dx, int dy, int dyText) {
		super();
		this.game = game;
		imageTransform = new PointTransform(X0 + x0, Y0 + y0 + dyText,     dx, dy + dyText);
		textTransform  = new PointTransform(X0 + x0, Y0 + y0 + dyText - 2, dx, dy + dyText);	
		defWidth = DEF_WIDTH;
		defHeight =  DEF_HEIGHT;
		
		isShowTitles = dyText > 0;
	}
	
	@Override
	public IGame getGame() {
		return this.game;
	}

	@Override
	public IImageStorage imageStorage() {
		return this.imageStorage;
	}

	@Override
	public IPointTransform imagePointTransform() {
		return imageTransform;
	}

	@Override
	public IPointTransform textPointTransform() {
		return textTransform;
	}
	
	IImageStorage imageStorage = new ImageStorageImpl(); 
	IGame game;
	
	IPointTransform imageTransform;
	IPointTransform textTransform;	
	int defWidth, defHeight;
	boolean isShowTitles;

	@Override
	public boolean isShowTitles() {
		return isShowTitles;
	}

	public void setShowTitles(boolean isShowTitles) {
		this.isShowTitles = isShowTitles;
	}

	@Override
	public int defaultWidth() {
		return defWidth;
	}

	@Override
	public int defaultHeight() {
		return defHeight;
	}

	@Override
	public void forEarch(Consumer<ISprite> consumer) {
		
		ArrayList<IEntity> sorted = new ArrayList<>();
		game.forEarchEntity(entity->{sorted.add(entity);});
		sorted.sort((a,b)->{return -( a.getDepth() - b.getDepth() );});
		sorted.forEach(entity->{
			ISprite sprite = new SpriteImpl(entity, this);
			consumer.accept(sprite);
		});
//		game.forEarchEntity(entity->{
//			ISprite sprite = new SpriteImpl(entity, this);
//			consumer.accept(sprite);
//		});
	}	
}
