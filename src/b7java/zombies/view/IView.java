package b7java.zombies.view;

import java.util.function.Consumer;

import b7java.zombies.entities.IGame;

public interface IView {
	
	IGame getGame();
	
	IImageStorage imageStorage();
	
	IPointTransform imagePointTransform();
	IPointTransform textPointTransform();
	
	int defaultWidth();
	int defaultHeight();
	
	void forEarch(Consumer<ISprite> consumer);
	
	boolean isShowTitles();
}
