package b7java.zombies.view;
import java.awt.image.BufferedImage;


public interface IImageStorage {
	BufferedImage image(String imageName);
}
