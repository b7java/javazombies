package b7java.zombies.view;

import java.awt.Graphics2D;
import java.awt.Image;
import b7java.zombies.geom.Point;
import java.awt.image.BufferedImage;

import b7java.zombies.entities.IEntity;
import b7java.zombies.images.SpriteData;

public class SpriteImpl implements ISprite {

	public SpriteImpl(IEntity entity, IView view) {
		super();
		this.entity = entity;
		this.view = view;
	}

	public Point imagePoint() {
		return view.imagePointTransform().transform(entity.getPoint());
	}

	public Point textPoint() {
		return view.textPointTransform().transform(entity.getPoint());
	}

	IEntity entity;
	IView	view;
	
	@Override
	public void paint(Graphics2D g2d) {
		paintImage(g2d);
		paintTitle(g2d);
	}

	private void paintImage(Graphics2D g2d) {
		SpriteData data = entity.getSpriteData();
		BufferedImage image = view.imageStorage().image(data.imageName);
		Point p = imagePoint();
		int x = (int) Math.round(p.x);
		int y = (int) Math.round(p.y);
		if (data.rect == null){
			int w = (int)(image.getWidth() * data.scaleX);
			int h = (int)(image.getHeight() * data.scaleY);
			if (data.scaleX < 0){
				x = x + Math.abs(w); 
			}
			if (data.scaleY < 0){
				y = y + Math.abs(h); 
			}			
			
			g2d.drawImage(image, x, y, w, h, null);
		}
		else{
			// https://docs.oracle.com/javase/tutorial/2d/images/drawimage.html	
			int w = data.rect.width;
			int h = data.rect.height;
			int dstW = (int)(data.rect.width * data.scaleX);
			int dstH = (int)(data.rect.height * data.scaleY);
			if (data.scaleX < 0){
				x = x + Math.abs(dstW); 
			}
			if (data.scaleY < 0){
				y = y + Math.abs(dstH); 
			}				
			g2d.drawImage(image, x, y, x + dstW, y + dstH,
						  data.rect.x, data.rect.y,
						  data.rect.x + w, 
						  data.rect.y + h, null);
		}
	}
	
	private void paintTitle(Graphics2D g2d) {
		if (!view.isShowTitles()){
			return;
		}
		String text = entity.getName();
		if (text != null && !text.isEmpty())
		{
			Point p = textPoint();
			g2d.drawString(text, (int) Math.round(p.x), (int) Math.round(p.y));
		}		
	}	
}
