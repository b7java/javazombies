package b7java.zombies.window;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import b7java.zombies.commands.IEventProcessor;

public class GameMouseListener implements MouseListener, MouseMotionListener {
	
	IEventProcessor processor;
	
	
	public GameMouseListener(IEventProcessor processor) {
		super();
		this.processor = processor;
	}

	@Override
	public void mouseClicked(MouseEvent mouseEvent) {
		processor.onMouse(mouseEvent);

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	

}
