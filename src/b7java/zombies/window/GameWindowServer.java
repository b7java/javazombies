package b7java.zombies.window;

import javax.swing.JComponent;

import b7java.zombies.connection.GameServer;
import b7java.zombies.entities.IGame;

/**
 * @author Cheskis Vladimir <vcheskis @ mail.ru>
 * @version 1.0
 * @since 1.1
 */

/**
 * The GameServer decorator, which repaints canvas after every step
 *
 */

public class GameWindowServer extends GameServer {

	public GameWindowServer(IGame game, JComponent canv) {
		super(game);
		this.canv = canv;
	}
	
	@Override
	public void step() {
		super.step();
		canv.repaint();
	}
	
	/** Canvas to redraw */
	JComponent canv;
}
