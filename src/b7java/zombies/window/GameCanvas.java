package b7java.zombies.window;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;

import b7java.zombies.entities.IGame;
import b7java.zombies.entities.IText;
import b7java.zombies.geom.Point;
import b7java.zombies.view.IPointTransform;
import b7java.zombies.view.IView;


public class GameCanvas extends JComponent {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -370745726688992561L;

	  public void paintComponent(Graphics g){
		  
		super.paintComponents(g);		
		Graphics2D g2d=(Graphics2D)g;
	 
		Font currentFont = g2d.getFont();
		Font newFont = currentFont.deriveFont(currentFont.getSize() * 1.5F);
		g2d.setFont(newFont);
		
		view.forEarch(sprite->sprite.paint(g2d));
		
		IGame game = view.getGame();
		game.forEarchText(text->paintText(text, g));

		g2d.setFont(currentFont);
		
		super.repaint();
	  }
	  
	  void paintText(IText text, Graphics g2d){
		  String str = text.getText();
		  if (str == null || str.isEmpty()){
			  return;
		  }
		  Point textPoint = text.getPoint();
		  IPointTransform transform = view.textPointTransform();
		  Point p = transform.transform(textPoint);
		  g2d.drawString(str, (int) Math.round(p.x), (int) Math.round(p.y));
	  }

	public GameCanvas(IView view) {
		super();
		this.view = view;
	}

	IView view;
}