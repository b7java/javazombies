package b7java.zombies.window;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.connection.IGameServer;
import b7java.zombies.entities.IGame;
import b7java.zombies.view.IView;
import b7java.zombies.view.DiscreteView;
 
public class GameWindow {

	public void showGame(IGame game)
	{
		IView view = new DiscreteView(game);
		showGame(view);
	}
	
	public void showGame(IView view)
	{
		GameCanvas canv= new GameCanvas(view);
		
		w=new JFrame("JavaZombies");

		w.setSize(view.defaultWidth(), view.defaultHeight());
	 
		w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 
		IGame game = view.getGame();
		IEventProcessor processor = game.getEventProcessor();
		if (processor != null){
			GameKeyboardListener keyBoardListener = new GameKeyboardListener(processor);
			w.addKeyListener(keyBoardListener);
			GameMouseListener mouseListener = new GameMouseListener(processor);
			w.addMouseListener(mouseListener);
		}
		
		
		w.setLayout(new BorderLayout(1,1));		
		
        w.add(canv);		
        w.setVisible(true);				
        
        currentGame = game;
        server = new GameWindowServer(game, canv);
        server.start();
	}
	
	public void setTitle(String title){
		w.setTitle(title);
	}
	
	JFrame w;
	IGameServer server;
	IGame currentGame;
}
