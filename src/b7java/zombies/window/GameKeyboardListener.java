package b7java.zombies.window;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import b7java.zombies.commands.IEventProcessor;


public class GameKeyboardListener implements KeyListener{
	
	

	@Override
	public void keyPressed(KeyEvent keyEvent) {
		processor.onKeyboard(keyEvent);
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}

	IEventProcessor processor;

	public GameKeyboardListener(IEventProcessor processor) {
		super();
		this.processor = processor;
	}
}
