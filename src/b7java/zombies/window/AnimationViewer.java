package b7java.zombies.window;
import java.awt.BorderLayout;

import javax.swing.JFrame;

import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.IAnimation;
 
public class AnimationViewer {
	
	public static void  show(IAnimation animation, AnimationPlayingSetting settings) throws Exception{
		

		JFrame w=new JFrame("Animation (Space: pause  on/off, right/left arrows - next/prev frame");

		w.setSize(800, 800);
	 
		w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 
		w.setLayout(new BorderLayout(1,1));		
		
		AnimationCanvas canv=new AnimationCanvas(animation, settings);
        w.add(canv);		
        w.setVisible(true);		
        AnimationViewerKeyboardListener keyListener = new AnimationViewerKeyboardListener(canv);
        w.addKeyListener(keyListener);
        
        w.update(canv.getGraphics());
	}
}
