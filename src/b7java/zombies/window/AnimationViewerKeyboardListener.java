package b7java.zombies.window;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class AnimationViewerKeyboardListener implements KeyListener{

	@Override
	public void keyPressed(KeyEvent keyEvent) {
		switch(keyEvent.getKeyCode()){
		case KeyEvent.VK_SPACE:
			canvas.togglePause();
			break;
		case KeyEvent.VK_RIGHT:
			canvas.doStep(1);
			break;
		case KeyEvent.VK_LEFT:
			canvas.doStep(-1);
			break;						
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}

	AnimationCanvas canvas;

	public AnimationViewerKeyboardListener(AnimationCanvas canvas) {
		super();
		this.canvas = canvas;
	}
}
