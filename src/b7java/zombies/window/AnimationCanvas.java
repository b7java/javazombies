package b7java.zombies.window;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JComponent;

import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.AnimationPlayingSetting.Mode;
import b7java.zombies.images.IAnimation;
import b7java.zombies.images.Rect;
import b7java.zombies.images.SpriteData;
import b7java.zombies.view.IImageStorage;
import b7java.zombies.view.ImageStorageImpl;

//http://grafika.me/node/285

public class AnimationCanvas extends JComponent{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -8954355645079316845L;
	private static final int Y0 = 10;
	private static final int X0 = 10;
	private static final int DY_TEXT = 20;
	private static final float PAUSE_SEC = 2;
	private static final int DY_FULL_IMAGE = 50;


	  public void paintComponent(Graphics g){
		super.paintComponents(g);		
		Graphics2D g2d=(Graphics2D)g;
	 

		SpriteData sprite = animation.getFrame(currentFrame);
		BufferedImage img = imageStorage.image(sprite.imageName);

		if (sprite.rect == null){
			g2d.drawImage(img, X0, Y0, null);
		}
		else{
			// https://docs.oracle.com/javase/tutorial/2d/images/drawimage.html	
			int w = sprite.rect.width;
			int h = sprite.rect.height;
			g2d.drawImage(img, X0, Y0, X0 + w, Y0 + h,
						  sprite.rect.x, sprite.rect.y,
						  sprite.rect.x + w, 
						  sprite.rect.y + h, null);
			drawRectOnFullImage(g2d, sprite, img);
		}

		drawFrameRect(g2d, sprite, img);
		drawFrameInfo(g2d, sprite, img);
	
		super.repaint();
	  }
	  
		
	  private void drawRectOnFullImage(Graphics2D g2d, SpriteData sprite, BufferedImage img) {
		  
		  Rect r = sprite.rect;
		  int x = X0;
		  int y = Y0 + r.height + DY_FULL_IMAGE;
		  g2d.drawImage(img, x, y, null);
		  g2d.drawRect(x, y, img.getWidth(), img.getHeight());
		  
		  Color prevColor = g2d.getColor();
		  g2d.setColor(Color.red);
		  g2d.drawRect(x + r.x, y + r.y, r.width, r.height);
		  g2d.setColor(Color.blue);
		  g2d.drawRect(x + r.x + 1, y + r.y + 1, r.width-2, r.height-2);		  
		  g2d.setColor(prevColor);
	  }


	private void drawFrameRect(Graphics2D g2d, SpriteData sprite, BufferedImage img) {
			int w = img.getWidth();
			int h = img.getHeight();
			if (sprite.rect != null){
				w = sprite.rect.width;
				h = sprite.rect.height;
			}
			g2d.drawRect(X0, Y0, w, h);			
	}

	private void drawFrameInfo(Graphics2D g2d, SpriteData sprite, BufferedImage img) {
			int h = img.getHeight();
			String text = "[" + currentFrame + "] " + sprite.imageName;
			if (sprite.rect != null){
				h = sprite.rect.height;
				text += " " + sprite.rect.toString();
			}
			g2d.drawString(text,
					       X0, Y0 + h + DY_TEXT );			
	}	  

	  IAnimation animation;
	  AnimationPlayingSetting settings;
	  Timer timer;
	  AnimationTimerTask task = new AnimationTimerTask(this);
		  
	  public AnimationCanvas(IAnimation animation, AnimationPlayingSetting settings) {
		super();
		this.animation = animation;
		this.settings = settings;
		
        timer = new Timer();
        int step = (int)(settings.dtSec * 1000);
        timer.schedule(task, step, step);
        
        pauseStepsMaxCount = (int)(PAUSE_SEC / settings.dtSec);

	}

	public void step(){
		if (isPause){
			return;
		}
		
		if (currentFrame < animation.getFramesCount() - 1){
			currentFrame++;
		}
		else{
			if (settings.mode == AnimationPlayingSetting.Mode.LOOP){
				currentFrame = 0;
			}
			else{
				pauseStepCount ++;
				if (pauseStepCount == pauseStepsMaxCount){
					currentFrame = 0;
					pauseStepCount = 0;
				}
			}
				
		}
	}

	IImageStorage imageStorage = new ImageStorageImpl("");  
	int currentFrame = 0;
	int pauseStepCount = 0;
	int pauseStepsMaxCount;
	
	boolean isPause= false;
	

	class AnimationTimerTask extends TimerTask {

		public AnimationTimerTask(AnimationCanvas canvas) {
			super();
			this.canvas = canvas;
		}
		@Override
		public void run() {
			canvas.step();
		}
		AnimationCanvas canvas;
	}


	public void togglePause() {
		isPause = !isPause;
	}


	public void doStep(int step) {
		isPause = true;
		currentFrame += step;
		int count = animation.getFramesCount();
		boolean isLoop = settings.mode == Mode.FIX_IN_END_FRAME;
		if (currentFrame < 0){
			if (isLoop){
				currentFrame = currentFrame + count;
			}
			else{
				currentFrame = 0;
			}
		}
		if (currentFrame >= count){
			if (isLoop){
				currentFrame = currentFrame - count;
			}
			else{
				currentFrame = count - 1;
			}			
		}
	}	
}