package b7java.zombies.exceptions;

/**
 * Something in configuration is wrong
 */
public class InvalidConfigException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5909427547194221246L;
	
	/**
	 * Problems
	 */	
	public enum Problem
	{
		COMMAND_IDS_REDEFENITION,
		COMMAND_NULL_POINTER
	}
	
	Problem problem;

	public InvalidConfigException(Problem problem) {
		super();
		this.problem = problem;
	}

	public InvalidConfigException(Problem problem, String message) {
		super(message);
		this.problem = problem;
	}

	public final Problem getProblem() {
		return problem;
	}
}
