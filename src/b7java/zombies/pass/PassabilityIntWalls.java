package b7java.zombies.pass;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import b7java.zombies.entities.IEntity;
import b7java.zombies.geom.CeilHelper;
import b7java.zombies.geom.IntPoint;
import b7java.zombies.geom.Point;

public class PassabilityIntWalls implements IPassability {

	public PassabilityIntWalls(Iterable<IEntity> wallEntities) {
		super();
		addWalls(wallEntities);
	}

	private void addWalls(Iterable<IEntity> wallEntities) {
		for(IEntity entity : wallEntities){
			addWall(entity.getPoint());
		}
	}

	public PassabilityIntWalls() {
		super();
	}

	@Override
	public boolean enabled(Point from, Point to) {
		return !CeilHelper.testNeighbours(to, walls::contains);
	}
	
	public void addWall(Point p){
		walls.add(IntPoint.round(p));
	}
	
	
	Set<IntPoint> walls = new HashSet<>();
}
