package b7java.zombies.pass;

import b7java.zombies.geom.Point;
import b7java.zombies.geom.Vector;

public interface IPassability {
	boolean enabled(Point from, Point to);
	
	default boolean enabled(Point from, Vector step){
		return enabled(from, from.moveByVector(step));
	}
}
