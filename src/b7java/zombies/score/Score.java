package b7java.zombies.score;

import b7java.zombies.entities.IText;

public class Score implements IScore {

	IText board;
	String prefix = "";
	int score = 0;
	
	public Score(IText board, String prefix, int score) {
		super();
		this.board = board;
		this.prefix = prefix;
		this.score = score;
		refreshBoard();
	}

	@Override
	public void inc() {
		score++;
		refreshBoard();
	}

	@Override
	public void dec() {
		score--;
		refreshBoard();
	}

	void refreshBoard(){
		if(board != null){
			String str = prefix + score;
			board.setText(str);
		}
	}

	@Override
	public int get() {
		return score;
	}
}
