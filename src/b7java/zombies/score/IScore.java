package b7java.zombies.score;

public interface IScore {
	int get();
	void inc();
	void dec();
}
