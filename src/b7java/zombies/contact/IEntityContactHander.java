package b7java.zombies.contact;

import b7java.zombies.entities.IEntity;

@FunctionalInterface
public interface IEntityContactHander{
	default boolean isEnd() { return false; }
	void accept(IEntity from, IEntity to);
}
