package b7java.zombies.contact;

import b7java.zombies.entities.IEntity;

public interface IEntityContactManager {
	
	void addTargetEntity(IEntity entity);
	boolean isEmpty();
	
	void contactTargets(IEntity from, 
						IEntityContactHander handler,
						boolean removeTargetEntities);
	
}
