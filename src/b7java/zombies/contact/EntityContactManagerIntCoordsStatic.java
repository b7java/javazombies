package b7java.zombies.contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;

import b7java.zombies.entities.IEntity;
import b7java.zombies.entities.IGame;
import b7java.zombies.geom.CeilHelper;
import b7java.zombies.geom.IntPoint;

public class EntityContactManagerIntCoordsStatic implements IEntityContactManager {

	@Override
	public void addTargetEntity(IEntity entity) {
		IntPoint p = IntPoint.round(entity.getPoint());
		List<IEntity> list = targets.get(p);
		if (list == null){
			list = new ArrayList<>();
			targets.put(p, list);
		}
		list.add(entity);
	}

	public EntityContactManagerIntCoordsStatic(IGame game) {
		super();
		this.game = game;
	}

	@Override
	public boolean isEmpty() {
		return targets.isEmpty();
	}

	@Override
	public void contactTargets(IEntity from, IEntityContactHander handler, boolean removeTargetEntities) {
		CeilHelper.testNeighbours(from.getPoint(), 
								  ip->{ 
									    contactTargets(from, ip, handler, removeTargetEntities);
									  	return false;
									  });
	}
	
	void contactTargets(IEntity from, IntPoint p,
						IEntityContactHander handler,
						boolean removeTargetEntities) {
		List<IEntity> list = targets.get(p);
		if (list == null){
			return;
		}		
		
		if (!removeTargetEntities){
			acceptTargets(from, handler, list);
		}
		else{
			acceptAndRemoveTargets(from, p, handler, list);
		}
	}

	private void acceptAndRemoveTargets(IEntity from, IntPoint p, IEntityContactHander handler, List<IEntity> list) {
		for(Iterator<IEntity> iterator = list.iterator(); iterator.hasNext() ;)
		{
			if (handler != null && handler.isEnd()){
				break;
			}
			IEntity target = iterator.next();
			if (handler !=  null){
				handler.accept(from, target);
			}
			game.remove(target);
			iterator.remove();
		}
		
		if (list.isEmpty()){
			targets.remove(p);
		}
	}

	private void acceptTargets(IEntity from, IEntityContactHander handler, List<IEntity> list) {
		if (handler !=  null){
			for(IEntity target : list){
				if (handler.isEnd()){
					break;
				}
				handler.accept(from, target);
			}
		}
	}	


	IGame game;
	Map<IntPoint, List<IEntity> > targets = new HashMap<>();
}
