package b7java.zombies.contact;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import b7java.zombies.entities.Game;
import b7java.zombies.entities.IEntity;
import b7java.zombies.geom.Point;

public class EntityContactManagerIntCoordsDinamic implements IEntityContactManager {

	public EntityContactManagerIntCoordsDinamic(Game game) {
		this.game = game;
	}

	@Override
	public void addTargetEntity(IEntity entity) {
		targets.add(entity);
	}

	@Override
	public boolean isEmpty() {
		return targets.isEmpty();
	}
	
	boolean checkPoint(Point from, Point to){
		return Math.abs(from.x - to.x) <= 1 && Math.abs(from.y - to.y) <= 1;
	}

	@Override
	public void contactTargets(IEntity from, IEntityContactHander handler, boolean removeTargetEntities) {
		Point pf = from.getPoint();
		for(Iterator<IEntity> iterator = targets.iterator(); iterator.hasNext() ;)
		{
			if (handler != null && handler.isEnd()){
				break;
			}
			IEntity target = iterator.next();
			
			if(!checkPoint(pf, target.getPoint())){
				continue;
			}
			
			if (handler !=  null){
				handler.accept(from, target);
			}

			if (removeTargetEntities){
				game.remove(target);
				iterator.remove();
			}
		}
	}
	
	Game game;
	List<IEntity> targets = new ArrayList<>();
}
