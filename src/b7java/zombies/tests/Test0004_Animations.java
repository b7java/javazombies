package b7java.zombies.tests;

import static org.junit.Assert.assertEquals;

import java.awt.Button;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import b7java.zombies.actions.AnimationAction;
import b7java.zombies.actions.IAction;
import b7java.zombies.actions.IAnimationAction;
import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.ICommand;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.events.IEvent;
import b7java.zombies.commands.events.KeyboardEventKey;
import b7java.zombies.commands.events.MouseEventKey;
import b7java.zombies.entities.Entity;
import b7java.zombies.exceptions.InvalidConfigException;
import b7java.zombies.images.AnimationFileHelper;
import b7java.zombies.images.AnimationPlayingSetting;
import b7java.zombies.images.IAnimation;
import b7java.zombies.images.Rect;
import b7java.zombies.images.SpriteData;

@RunWith(MockitoJUnitRunner.class)
public class Test0004_Animations {

    
	@Test
	public void testAnimationFromFiles() {
		String preffix = "FW_Demon_Lord_Attack_Flame__";
		String folder = "animations/Demon_Lord";
		Rect rect = new Rect(100, 100, 50, 50);
		IAnimation animation = AnimationFileHelper.animationFromFiles(folder, preffix, rect);
		
		assertEquals(30, animation.getFramesCount());
		
		SpriteData sprite = animation.getFrame(0);
		assertEquals("animations/Demon_Lord/FW_Demon_Lord_Attack_Flame__000.png", sprite.imageName);
		assertEquals(rect, sprite.rect);
	}

	@Test
	public void testAnimationFromSheet() {
		String imageName = "animations/Demon_Lord_Sheets/FW_Demon_Lord_Running.png";
		IAnimation animation = AnimationFileHelper.animationFromSheet(imageName, 
																	  1482, 1064,
																	  6, 4);
		Rect rect = new Rect(0, 0, 1482 / 6, 1064 / 4);
		
		assertEquals(24, animation.getFramesCount());
		
		SpriteData sprite = animation.getFrame(0);
		assertEquals("animations/Demon_Lord_Sheets/FW_Demon_Lord_Running.png", sprite.imageName);
		assertEquals(rect, sprite.rect);
	}	
	
	@Test
	public void testAnimationActionFIX_IN_END_FRAME(){
	
		IAnimationAction action = createAnimationAction(AnimationPlayingSetting.Mode.FIX_IN_END_FRAME);
		float dtSec = AnimationPlayingSetting.DEFAULT_STEP;
		

		IAction.State state = action.step(dtSec);
		
		assertEquals(IAction.State.ACTIVE, state);
		assertEquals("animations/Demon_Lord/FW_Demon_Lord_Attack_Flame__000.png", action.getImage().imageName);

		state = action.step(dtSec);
		
		assertEquals(IAction.State.ACTIVE, state);
		assertEquals("animations/Demon_Lord/FW_Demon_Lord_Attack_Flame__001.png", action.getImage().imageName);
		
		// animation has 30 frames
		state = action.step(30 * dtSec);

		assertEquals("animations/Demon_Lord/FW_Demon_Lord_Attack_Flame__029.png", action.getImage().imageName);
		assertEquals(IAction.State.TO_BE_DELETED, state);
	}
	
	@Test
	public void testAnimationActionLOOP(){
	
		IAnimationAction action = createAnimationAction(AnimationPlayingSetting.Mode.LOOP);
		float dtSec = AnimationPlayingSetting.DEFAULT_STEP;
		

		IAction.State state = action.step(dtSec);
		
		assertEquals(IAction.State.ACTIVE, state);
		assertEquals("animations/Demon_Lord/FW_Demon_Lord_Attack_Flame__000.png", action.getImage().imageName);

		state = action.step(dtSec);
		
		assertEquals(IAction.State.ACTIVE, state);
		assertEquals("animations/Demon_Lord/FW_Demon_Lord_Attack_Flame__001.png", action.getImage().imageName);
		
		// animation has 30 frames
		state = action.step(30 * dtSec);

		assertEquals("animations/Demon_Lord/FW_Demon_Lord_Attack_Flame__001.png", action.getImage().imageName);
		assertEquals(IAction.State.ACTIVE, state);
	}
	
	private IAnimationAction createAnimationAction(AnimationPlayingSetting.Mode mode){
		Entity entity = new Entity("Jon", 2, 2);

		AnimationPlayingSetting settings = new AnimationPlayingSetting(mode);
		String preffix = "FW_Demon_Lord_Attack_Flame__";
		String folder = "animations/Demon_Lord";
		IAnimation animation = AnimationFileHelper.animationFromFiles(folder, preffix);
		assertEquals(30, animation.getFramesCount());

		return new AnimationAction(entity, animation, settings);		
	}
}
