package b7java.zombies.tests;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import b7java.zombies.actions.ActionList;
import b7java.zombies.collision.CollisionDetectorByCoord;
import b7java.zombies.collision.CollisionDetectorWithCache;
import b7java.zombies.collision.CollisionManager;
import b7java.zombies.collision.ICollisionDetector;
import b7java.zombies.collision.ICollisionHander;
import b7java.zombies.collision.ICollisionManager;
import b7java.zombies.commands.ICommand;
import b7java.zombies.commands.events.IEvent;
import b7java.zombies.entities.Entity;

@RunWith(MockitoJUnitRunner.class)
public class Test0022_Collision {

	Entity e00 = new Entity("00", 0, 0);
	Entity e00a = new Entity("00a", 0, 0);
	Entity e01 = new Entity("01", 0, 1);
	Entity e11 = new Entity("11", 1, 1);
	Entity e21 = new Entity("21", 2, 1);
	
    @Mock 
    ICollisionDetector mdetector;
    
    @Mock
    ICollisionHander mhandler;
	
	@Test
	public void testCollisionDetectorByCoord() {
		
		CollisionDetectorByCoord coordEq = new CollisionDetectorByCoord(CollisionDetectorByCoord.Neighborhood.EQ);
		
		assertEquals(true, coordEq.areCollide(e00,  e00a));		
		assertEquals(false, coordEq.areCollide(e00,  e01));		
		assertEquals(false, coordEq.areCollide(e00,  e11));		
		assertEquals(false, coordEq.areCollide(e00,  e21));		
		
		CollisionDetectorByCoord coord4 = new CollisionDetectorByCoord(CollisionDetectorByCoord.Neighborhood.NEIGHBOURS_4);
		
		assertEquals(true, coord4.areCollide(e00,  e00a));		
		assertEquals(true, coord4.areCollide(e00,  e01));		
		assertEquals(false, coord4.areCollide(e00,  e11));				
		assertEquals(false, coord4.areCollide(e00,  e21));		

		CollisionDetectorByCoord coord8 = new CollisionDetectorByCoord(CollisionDetectorByCoord.Neighborhood.NEIGHBOURS_8);
		
		assertEquals(true, coord8.areCollide(e00,  e00a));		
		assertEquals(true, coord8.areCollide(e00,  e01));		
		assertEquals(true, coord8.areCollide(e00,  e11));				
		assertEquals(false, coord8.areCollide(e00,  e21));		
	}

	@Test
	public void testCollisionDetectorWithCache() {
		CollisionDetectorWithCache detector = new CollisionDetectorWithCache(mdetector);
		detector.areCollide(e00, e00a);
		detector.areCollide(e00, e00a);
		
		Mockito.verify(mdetector, Mockito.times(1)).areCollide(e00, e00a);

		detector.clearCache();
		detector.areCollide(e00, e00a);
		Mockito.verify(mdetector, Mockito.times(2)).areCollide(e00, e00a);
	}

	@Test
	public void testCollisionManager(){
	
		CollisionDetectorByCoord coordEq = new CollisionDetectorByCoord(CollisionDetectorByCoord.Neighborhood.EQ);

		ICollisionManager manager = new CollisionManager();
		manager.setDetector(coordEq, false);
		manager.addHandler(mhandler);
		
		manager.step();
		
		Mockito.verify(mhandler, Mockito.times(1)).process(coordEq);
	}
}
