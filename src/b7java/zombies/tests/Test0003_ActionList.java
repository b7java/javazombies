package b7java.zombies.tests;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import b7java.zombies.actions.ActionList;

public class Test0003_ActionList {

	@Test
	public void test() {
		
		StringBuilder target = new StringBuilder();
		ActionList actions = new ActionList();
		
		actions.add(new TestActionStringAding(Arrays.asList("A", "A", "A"), target));
		actions.add(new TestActionStringAding(Arrays.asList("B"), target));
		actions.add(new TestActionStringAding(Arrays.asList("C", "C"), target));
		
		actions.step(1);
		assertEquals("ABC", target.toString());
		assertEquals(2, actions.getActions().size());
		
		target.delete(0, target.length());
		actions.step(1);
		assertEquals("AC", target.toString());
		assertEquals(1, actions.getActions().size());		
		
		target.delete(0, target.length());
		actions.step(1);
		assertEquals("A", target.toString());	
		assertEquals(0, actions.getActions().size());		
		
	}

}
