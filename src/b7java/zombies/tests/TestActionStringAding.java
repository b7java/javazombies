package b7java.zombies.tests;

import java.util.List;

import b7java.zombies.actions.IAction;

public class TestActionStringAding implements IAction {

	@Override
	public State step(float dtSec) {
		if (current < strings.size()){
			target.append(strings.get(current));
			current ++;
			return current < strings.size() ? State.ACTIVE : State.TO_BE_DELETED;
		}
		return State.TO_BE_DELETED;
	}

	public TestActionStringAding(List<String> strings, StringBuilder target) {
		super();
		this.strings = strings;
		this.target = target;
	}

	List<String>	strings;
	StringBuilder	target;
	int			 	current = 0;
}
