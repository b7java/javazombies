package b7java.zombies.tests;

import java.awt.Button;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import b7java.zombies.commands.EventProcessor;
import b7java.zombies.commands.ICommand;
import b7java.zombies.commands.IEventProcessor;
import b7java.zombies.commands.events.IEvent;
import b7java.zombies.commands.events.KeyboardEventKey;
import b7java.zombies.commands.events.MouseEventKey;
import b7java.zombies.exceptions.InvalidConfigException;

@RunWith(MockitoJUnitRunner.class)
public class Test0002_EventProcessor {

    @Mock 
    ICommand mcommand;
    IEventProcessor processor = new EventProcessor();
	Button button = new Button("click");
    
    @Before
    public void init() throws InvalidConfigException{
		processor.add("Test", mcommand);
		Mockito.when(mcommand.isEnabled()).thenReturn(true);
    }
    
	@Test
	public void testKeyEvent() throws InvalidConfigException {
		
		processor.bind("Test", new KeyboardEventKey(KeyEvent.VK_SPACE, false, false, false));
		KeyEvent e = new KeyEvent(button, 1, System.currentTimeMillis(), 0, KeyEvent.VK_SPACE, ' ');
		
		processor.onKeyboard(e);
	
		Mockito.verify(mcommand).run(Mockito.any(IEvent.class));
	}

	@Test
	public void testMouse() throws InvalidConfigException {
		
		processor.bind("Test", new MouseEventKey(MouseEvent.BUTTON1));
		MouseEvent e = new MouseEvent(button, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), MouseEvent.BUTTON1_MASK, 100, 100, 1, false);
		
		processor.onMouse(e);
	
		Mockito.verify(mcommand).run(Mockito.any(IEvent.class));
	}	
}
