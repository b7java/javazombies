package b7java.zombies.tests;

import static org.junit.Assert.*;

import b7java.zombies.geom.Point;

import org.junit.BeforeClass;
import org.junit.Test;

import b7java.zombies.actions.RunAction;
import b7java.zombies.entities.Entity;

public class Test0008_RunAction {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	/**
	 * The test RunAction Rectangle (1)
	 * <p>
	 * We test the movement of the entity along the hypotenuse of a right triangle .
	 * (2)
	 */
	@Test
	public void testRectangle() {
		Entity entity = new Entity("zomb", 5, 5);
		Point point = new Point(10, 10);
		float k = 5;
		RunAction action = new RunAction(entity, point, k);
		assertEquals(5, (int) entity.getPoint().getX());
		assertEquals(5, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(6, (int) entity.getPoint().getX());
		assertEquals(6, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(7, (int) entity.getPoint().getX());
		assertEquals(7, (int) entity.getPoint().getY());
		action.step(2);
		assertEquals(9, (int) entity.getPoint().getX());
		assertEquals(9, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(10, (int) entity.getPoint().getX());
		assertEquals(10, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(10, (int) entity.getPoint().getX());
		assertEquals(10, (int) entity.getPoint().getY());
	}

	/**
	 * The test RunAction square (1)
	 * <p>
	 * We test the movement of the entity along the hypotenuse of an isosceles
	 * triangle. (2)
	 */
	@Test
	public void testSquare() {
		Entity entity = new Entity("zomb", 5, 5);
		Point point = new Point(10, 10);
		float k = 5;
		RunAction action = new RunAction(entity, point, k);
		assertEquals(5, (int) entity.getPoint().getX());
		assertEquals(5, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(6, (int) entity.getPoint().getX());
		assertEquals(6, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(7, (int) entity.getPoint().getX());
		assertEquals(7, (int) entity.getPoint().getY());
		action.step(2);
		assertEquals(9, (int) entity.getPoint().getX());
		assertEquals(9, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(10, (int) entity.getPoint().getX());
		assertEquals(10, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(10, (int) entity.getPoint().getX());
		assertEquals(10, (int) entity.getPoint().getY());
	}

	/**
	 * The test RunAction of the line (1)
	 * <p>
	 * We test the moving of the entity along one of the coordinate axes. (2)
	 */
	@Test
	public void testLine() {
		Entity entity = new Entity("zomb", 5, 5);
		Point point = new Point(5, 10);
		float k = 5;
		RunAction action = new RunAction(entity, point, k);
		assertEquals(5, (int) entity.getPoint().getX());
		assertEquals(5, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(5, (int) entity.getPoint().getX());
		assertEquals(6, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(5, (int) entity.getPoint().getX());
		assertEquals(7, (int) entity.getPoint().getY());
		action.step(2);
		assertEquals(5, (int) entity.getPoint().getX());
		assertEquals(9, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(5, (int) entity.getPoint().getX());
		assertEquals(10, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(5, (int) entity.getPoint().getX());
		assertEquals(10, (int) entity.getPoint().getY());
	}

	/**
	 * The test RunAction of the Time = 0 (1)
	 * <p>
	 * We test the movement of the entity with a travel time of 0. (2)
	 */
	@Test
	public void testTimeZero() {
		Entity entity = new Entity("zomb", 5, 5);
		Point point = new Point(10, 6);
		float k = 0;
		RunAction action = new RunAction(entity, point, k);
		assertEquals(5, (int) entity.getPoint().getX());
		assertEquals(5, (int) entity.getPoint().getY());
		action.step(1);
		assertEquals(10, (int) entity.getPoint().getX());
		assertEquals(6, (int) entity.getPoint().getY());
	}
}