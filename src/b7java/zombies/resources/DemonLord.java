package b7java.zombies.resources;

import b7java.zombies.geom.Axis;
import b7java.zombies.images.AnimationFileHelper;
import b7java.zombies.images.AnimationInverted;
import b7java.zombies.images.IAnimation;
import b7java.zombies.images.SpriteData;

public class DemonLord {
	static IAnimation walk;
	static IAnimation walkInverted;
	
	static void initAnimations(){
		if (walk == null){
			String imageName = "animations/Demon_Lord_Sheets/FW_Demon_Lord_Walking.png";
			int width = 1338;
			int height = 1698;
			int columns = 6;
			int rows = 6;
			walk = AnimationFileHelper.animationFromSheet(imageName , width, height, columns, rows);
			walkInverted = new AnimationInverted(walk, Axis.X);
		}
	}
	
	public static SpriteData idleSprite(){
		return moveRight().getFrame(0);
	}
	
	public static IAnimation moveRight(){
		initAnimations();
		return walk;
	}
	
	public static IAnimation moveLeft(){
		initAnimations();
		return walkInverted;
	}	
}
