package b7java.zombies.resources;

import b7java.zombies.images.AnimationFileHelper;
import b7java.zombies.images.IAnimation;

public class Packman {
	private static final int ROWS = 4;
	private static final int COLUMNS = 14;
	private static final String IMAGE_NAME = "animations/Packman/packman1.png";
	private static final int WIDTH = 448;
	private static final int HEIGHT = 128;

	public static IAnimation allFrames(){
		if (allFramesAnimation == null){
			allFramesAnimation = AnimationFileHelper.animationFromSheet(IMAGE_NAME, 
																		WIDTH, HEIGHT,
																		COLUMNS, ROWS);
		}
		return allFramesAnimation;
	}
	
	public static IAnimation moveRight(){
		return getAnimation2Frames(10, 0);
	}

	public static IAnimation moveDown(){
		return getAnimation2Frames(10, 1);
	}
		
	public static IAnimation moveLeft(){
		return getAnimation2Frames(10, 2);
	}
	
	public static IAnimation moveUp(){
		return getAnimation2Frames(10, 3);
	}
	
	public static IAnimation ghostRight(){
		return getAnimation2Frames(0, 0);
	}

	public static IAnimation ghostDown(){
		return getAnimation2Frames(0, 1);
	}
		
	public static IAnimation ghostLeft(){
		return getAnimation2Frames(0, 2);
	}
	
	public static IAnimation ghostUp(){
		return getAnimation2Frames(0, 3);
	}
	
	private static IAnimation getAnimation2Frames(int columnStart, int rowStart){
		int [] frames = {frame(columnStart, rowStart), frame(columnStart + 1, rowStart)}; 
		return AnimationFileHelper.animationFromSheetFrames(allFrames(), frames);
	}	
	
	public static int spriteWidth() {
		return WIDTH / COLUMNS;
	}
	
	public static int spriteHeight() {
		return HEIGHT / ROWS;
	}
	
	public static int frame(int column, int row){
		return row * 14 + column;
	}
	
	public static int frameWall(){
		return frame(13,3);
	}
	
	public static int frameFood(){
		return frame(12,3);
	}	
	
	public static int frameMan(){
		return frame(11,0);
	}		

	public static int frameGhostRed(){
		return frame(0,0);
	}		
		
	static IAnimation allFramesAnimation;
}
